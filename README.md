# Personal Portfolio
Welcome to a small collection of my coding projects! The intent of this repo is to demonstrate my abilities and understanding of software development techniques and tools. These projects are personal or academic projects that highlight my strengths.

### Lunar Landing
A client side javascript game.

### Tower Defense
A client side javascript game, but served from a server for faster menuing.

### Parallel Pi
A demonstration of a worker/boss parallel program, that calculates pi using all possible local cpu cores.

### Decision Tree
Builds a small decision tree using the ID3 algorithm from a data set determining "Should I play tennis?"

### Command Line Viewable Snake Tank Monitoring System
Images for my pet snake terrarium monitoring system.

