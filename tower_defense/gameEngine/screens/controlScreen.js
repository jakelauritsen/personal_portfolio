TD.controlScreen = (function(state){

    COORD_SIZE = 1000;
    state.title = "Control Settings";
    state.subTitle = "Click key binding and press new key";
    state.listening = false;
    state.changingButtonIndex = null;
    state.keyStrokes = []


    state.buttons = [];
    let button_offset = 0;
    for(let i = 0; i < state.controls.length; i++){
        state.buttons.push({
            name:state.controls[i].name,
            key:state.controls[i].key,
            x:(COORD_SIZE/2)-100,
            y:(COORD_SIZE/2)-55 + button_offset,
            width:200,
            height:35,
        });
        button_offset+=45;
    }

    state.navMenuButton = {
        name:"Back to Menu",
        x:(COORD_SIZE/2)-100,
        y:(COORD_SIZE/2)-55 + button_offset,
        width:200,
        height:35,
    }


    function render(context){
        Graphics.controls.render(context);
    }

    function update(elapsedTime){
        Drivers.controls.update(elapsedTime, state);
    }
    // Now all update methods
    return {
        update,
        render,
    }
}(TD.controlState));