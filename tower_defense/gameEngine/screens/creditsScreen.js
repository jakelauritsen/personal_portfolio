TD.creditsScreen = (function(state){
    canvas = document.getElementById('canvas-main');
    context = canvas.getContext('2d');

    COORD_SIZE = 1000;

    state.title = "Credits For My Glory";
    state.subTitle = "Masterfully crafted by Jake Lauritsen";
    state.menuButton = {
        name:"Return to Menu",
        x:(COORD_SIZE/2)-75,
        y:(COORD_SIZE/2)-100,
        width:150,
        height: 35
    }


    function update(elapsedTime){
        Drivers.credits.update(elapsedTime);
    }
    function render(context){
        Graphics.credits.render(context);
    }
    // Now all update methods
    return {
        update,
        render,
    }
}(TD.creditState));