TD.scoreScreen = (function(state){
    canvas = document.getElementById('canvas-main');
    context = canvas.getContext('2d');

    COORD_SIZE = 1000;

    state.title = "Best Grub Killer Records";


    state.menuButton = {
        name:"Return to Menu",
        x:(COORD_SIZE/2)-75,
        y:(COORD_SIZE/2)+20,
        width:150,
        height: 35
    }


    function update(elapsedTime){
        Drivers.scores.update(elapsedTime);
    }
    function render(context){
        Graphics.scores.render(context);
    }
    // Now all update methods
    return {
        update,
        render,
    }
}(TD.scoreState));