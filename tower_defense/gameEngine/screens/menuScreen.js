TD.menuScreen = (function(state){

    // As the first loaded screen I will add the input to component here
    Components.mouseInput = Components.input.mouse();

    COORD_SIZE = 1000;
    state.title = "Urban Grub Attack!";

    state.buttons = [{
        "name":"New Game",
        "x":(COORD_SIZE/2)-75,
        "y":(COORD_SIZE/2)-100,
        "width":150,
        "height": 35,
    },
    {
        "name":"High Scores",
        "x":(COORD_SIZE/2)-75,
        "y":(COORD_SIZE/2)-55,
        "width":150,
        "height": 35,
    },
    {
        "name":"Controls",
        "x":(COORD_SIZE/2)-75,
        "y":(COORD_SIZE/2)-10,
        "width":150,
        "height": 35,
    },
    {
        "name":"Credits",
        "x":(COORD_SIZE/2)-75,
        "y":(COORD_SIZE/2)+35,
        "width":150,
        "height": 35,
    }];

    function render(context){
        Graphics.menu.render(context);
    }

    function update(elapsedTime){
        Drivers.menu.update(elapsedTime, state);
    }
    // Now all update methods
    return {
        update,
        render,
    }
}(TD.menuState));