TD.gameScreen = (function(state){
    COORD_SIZE = 1000;


    function update(elapsedTime){
        Drivers.game.update(elapsedTime);
    }
    function render(context){
        Graphics.game.render(context);
    }
    // Now all update methods
    return {
        update,
        render,
    }
}(TD.gameState));