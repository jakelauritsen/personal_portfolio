TD.testPage = (function(state){
    state.count=0;
    state.recordedTime=0;

    function render(state){
        Graphics.testPageRenderer.render(state);
    }

    function update(elapsedTime){
        Drivers.testPageUpdater.update(elapsedTime, state);
    }
    // Now all update methods
    return {
        update,
        render,
    }
}(TD.testState));

