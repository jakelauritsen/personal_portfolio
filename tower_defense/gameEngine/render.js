Graphics.render = (function(){
    let canvas = document.getElementById('canvas-main');
    let context = canvas.getContext('2d');
    // The render needs to recieve the game model to me this means the state of the game after a update wrapped into a js object
    function masterRender(){
        context.clearRect(0, 0, canvas.width, canvas.height);

        TD.currentScreen.render(context);
    }
    
    return {masterRender}

}());

