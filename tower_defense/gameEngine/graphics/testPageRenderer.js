Graphics.testPageRenderer = (function(state){

    let canvas = document.getElementById('canvas-main');
    let context = canvas.getContext('2d');
    // The render needs to recieve the game model to me this means the state of the game after a update wrapped into a js object
    
    function render(){
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.save();
        context.font = "50px Sans-Serif";
        context.fillStyle = 'rgb(0, 0, 0)';
        context.textAlign = "center";
        // console.log(state)
        context.fillText(state.count, canvas.width/2, canvas.height/2);
        context.restore();
    }
    
    return {render}

}(TD.testState));
