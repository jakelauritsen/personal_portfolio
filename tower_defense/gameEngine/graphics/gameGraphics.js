Graphics.game = (function(state){
    // This must be the first thing in hte update method to preserve the processInput -> update -> render, model
    // processInput();

    // let state = LunarLanding.state;
    let COORD_SIZE = 1000

    // load background
    let background = new Image();
    background.isReady = false;

    background.onload = function() {
        this.isReady = true;
    };
    
    background.src = "gameEngine/images/gameboard_background.jpg";

    // load turret platform
    let turret_platform = new Image();
    turret_platform.isReady = false;

    turret_platform.onload = function() {
        this.isReady = true;
    };

    turret_platform.src = "gameEngine/images/turrets/platform.png";
    
    

    function render(context){
        // Render the background
        if (background.isReady) {
            context.drawImage(background, 0, 0, COORD_SIZE , COORD_SIZE);
        }

        if(state.initialized == true){
            // This is the band for the stats to be on.
            context.save();
            context.fillStyle = "rgb(0,0,0)";
            context.fillRect(0, COORD_SIZE/50, COORD_SIZE, 40);
            context.strokeStyle = "rgb(255,255,255)";
            context.strokeRect(0, COORD_SIZE/50, COORD_SIZE, 40);
            context.restore();

            // This presents the stats on the banner
            context.save();
            context.strokeStyle = 'rgb(255, 255, 255)';
            context.lineWidth = 6;
            context.font = "30px Didot, serif";
            context.fillStyle = 'rgb(255, 255, 255)';
            context.textAlign = "center";

            context.fillText("Level: " + state.stats.level.toString(), COORD_SIZE*.20, COORD_SIZE*.05); 
            context.fillText("Lives: " + state.stats.lives.toString(), COORD_SIZE*.40, COORD_SIZE* .05); 
            context.fillText("Score: " + state.stats.score.toString(), COORD_SIZE*.60, COORD_SIZE*.05); 
            context.fillText("Cash: " + state.stats.cash.toString(), COORD_SIZE*.80, COORD_SIZE*.05); 
            
            context.restore();

            // This is the band for the store
            context.save();
            context.fillStyle = "rgb(255,255,255,.3)";
            context.fillRect(0, COORD_SIZE - COORD_SIZE*.08, COORD_SIZE, 65);
            context.strokeStyle = "rgb(0,0,0)";
            context.lineWidth = 3;
            context.strokeRect(0, COORD_SIZE - COORD_SIZE*.08 , COORD_SIZE, 65);
            context.restore();

            // This presents the store options, turrets, sale selected turret, upgrade selected turret.
            context.save();
            context.strokeStyle = 'rgb(0,0,0)';
            context.lineWidth = 6;
            context.font = "24px Didot, serif";
            context.fillStyle = 'rgb(0, 0, 0)';
            context.textAlign = "center";

            context.fillText("Market", COORD_SIZE*.08, COORD_SIZE*.957); 
            context.restore();

            context.save();
            context.strokeStyle = "rgb(0,0,0)";
            context.lineWidth = 3;
            context.strokeRect(0, COORD_SIZE - COORD_SIZE*.08 , COORD_SIZE*.15, 65);
            context.restore();

            let offset = COORD_SIZE*.15-65;
            for(let i = 0; i < state.store.merch.length; i++){
                if (turret_platform.isReady) {
                    context.drawImage(turret_platform, state.store.merch[i].box.x, state.store.merch[i].box.y, state.store.merch[i].box.width, state.store.merch[i].box.height);
                }

                if (state.store.merch[i].turret.image.isReady) {
                    context.drawImage(state.store.merch[i].turret.image, state.store.merch[i].box.x, state.store.merch[i].box.y, state.store.merch[i].box.width, state.store.merch[i].box.height);
                }

                // I need to replace these rectangles with the turret images
                // context.save();
                // context.fillStyle = "rgb(255,255,255,.3)";
                // context.fillRect(offset + 65, COORD_SIZE - COORD_SIZE*.08, 65, 65);

                // context.strokeStyle = "rgb(255,255,255,.6)";
                // context.strokeRect(offset + 65, COORD_SIZE - COORD_SIZE*.08, 65, 65);
                // context.restore();

                // I need to overlay the turret holder here with price
                context.save();
                context.strokeStyle = 'rgb(255,255,255)';
                context.lineWidth = 6;
                context.font = "16px Didot, serif";
                context.fillStyle = 'rgb(255,255,255)';
                context.textAlign = "center";
        
                context.fillText(state.store.merch[i].turret.price, COORD_SIZE*.08+offset+17, COORD_SIZE*.957); 
                context.restore();

                offset += 65
            }

            // I need to render the upgrade and sell rectangles
            for(let i = 0; i < state.store.features.length; i++){
                context.save();
                context.fillStyle = "rgb(255,255,255,.3)";
                context.fillRect(state.store.features[i].box.x, state.store.features[i].box.y, state.store.features[i].box.width, state.store.features[i].box.height);

                context.strokeStyle = "rgb(255,255,255,.6)";
                context.strokeRect(state.store.features[i].box.x, state.store.features[i].box.y, state.store.features[i].box.width, state.store.features[i].box.height);
                context.restore();

                context.save();
                context.strokeStyle = 'rgb(255,255,255)';
                context.lineWidth = 6;
                context.font = "16px Didot, serif";
                context.fillStyle = 'rgb(255,255,255)';
                context.textAlign = "center";
        
                context.fillText(state.store.features[i].name, COORD_SIZE*.08+offset+17, COORD_SIZE*.957); 
                context.restore();

                offset += 65
            }


            // This renders the board for testing purposes, actually I like playing with it. It might stay
            offset = state.dimensions.offset;
            for(let r = 0; r < state.board.length; r++){
                for(let cell = 0; cell < state.board.length; cell++){
                    if (state.board[r][cell].turret !== null){
                        if (turret_platform.isReady) { 
                            context.drawImage(turret_platform, offset + (cell*state.board[r][cell].box.width), offset + (r*state.board[r][cell].box.height), state.board[r][cell].box.width, state.board[r][cell].box.height);
                        }
            
                        if (state.board[r][cell].turret.image.isReady) {

                            context.save();
                            // translate to the center of the turret cell
                            context.translate((offset + (cell*state.board[r][cell].box.width))+state.dimensions.cellSize/2, offset + (r*state.board[r][cell].box.height)+state.dimensions.cellSize/2);
                            context.rotate(state.board[r][cell].turret.theta);
                            context.drawImage(state.board[r][cell].turret.image, -1*state.dimensions.cellSize/2, -1*state.dimensions.cellSize/2, state.board[r][cell].box.width, state.board[r][cell].box.height);
                            context.restore();
                            
                            // context.drawImage(state.board[r][cell].turret.image, offset + (cell*state.board[r][cell].box.width), offset + (r*state.board[r][cell].box.height), state.board[r][cell].box.width, state.board[r][cell].box.height);
                        }

                        if(state.board[r][cell].selected){
                            context.save();
                            context.lineWidth = 6;
                            context.strokeStyle = "rgb(255,255,255)";
                            context.strokeRect(offset + (cell*state.board[r][cell].box.width), offset + (r*state.board[r][cell].box.height), state.board[r][cell].box.width, state.board[r][cell].box.height);
            
                            context.restore();
                        }

                    }
                    else{
                        if(state.board[r][cell].onShortPath == false){
                            context.save();
                            context.fillStyle = "rgb(255,255,255,.3)";
                            context.fillRect(offset + (cell*state.board[r][cell].box.width), offset + (r*state.board[r][cell].box.height), state.board[r][cell].box.width, state.board[r][cell].box.height);

                            context.strokeStyle = "rgb(255,255,255,.6)";
                            context.strokeRect(offset + (cell*state.board[r][cell].box.width), offset + (r*state.board[r][cell].box.height), state.board[r][cell].box.width, state.board[r][cell].box.height);

                            context.restore();
                        }
                        else{
                            context.save();
                            context.fillStyle = "rgb(0,0,255,.2)";
                            context.fillRect(offset + (cell*state.board[r][cell].box.width), offset + (r*state.board[r][cell].box.height), state.board[r][cell].box.width, state.board[r][cell].box.height);

                            context.strokeStyle = "rgb(0,0,255,.4)";
                            context.strokeRect(offset + (cell*state.board[r][cell].box.width), offset + (r*state.board[r][cell].box.height), state.board[r][cell].box.width, state.board[r][cell].box.height);

                            context.restore();
                        }
                    }
                }
            }

            // Render a test rectangle for spawn points 
            // context.save();
            // context.strokeStyle = "rgb(255,255,255,.6)";
            // context.strokeRect(state.spawnPoints.N.box.x, state.spawnPoints.N.box.y, state.spawnPoints.N.box.width, state.spawnPoints.N.box.height);
            // context.strokeRect(state.spawnPoints.W.box.x, state.spawnPoints.W.box.y, state.spawnPoints.W.box.width, state.spawnPoints.W.box.height);
            // context.strokeRect(state.spawnPoints.E.box.x, state.spawnPoints.E.box.y, state.spawnPoints.E.box.width, state.spawnPoints.E.box.height);
            // context.strokeRect(state.spawnPoints.S.box.x, state.spawnPoints.S.box.y, state.spawnPoints.S.box.width, state.spawnPoints.S.box.height);
            // context.restore();

            // This will render the game border up to the barriers
            //top-left corner
            context.save();
            context.beginPath();
            context.strokeStyle = 'rgb(0, 0, 0)';
            context.lineWidth = 6;
            context.moveTo(offset, offset);
            context.lineTo(offset, offset+(5*state.dimensions.cellSize))
            context.moveTo(offset, offset);
            context.lineTo(offset+(5*state.dimensions.cellSize), offset)

            context.stroke();
            context.restore();

            //top-right corner
            context.save();
            context.beginPath();
            context.strokeStyle = 'rgb(0, 0, 0)';
            context.lineWidth = 6;
            context.moveTo(COORD_SIZE - offset, offset);
            context.lineTo(COORD_SIZE - offset - (5*state.dimensions.cellSize), offset)
            context.moveTo(COORD_SIZE - offset, offset);
            context.lineTo(COORD_SIZE - offset, offset+(5*state.dimensions.cellSize))

            context.stroke();
            context.restore();

            //bottom-left corner
            context.save();
            context.beginPath();
            context.strokeStyle = 'rgb(0, 0, 0)';
            context.lineWidth = 6;
            context.moveTo(offset, COORD_SIZE - offset);
            context.lineTo(offset, COORD_SIZE - offset - (5*state.dimensions.cellSize))
            context.moveTo(offset, COORD_SIZE - offset);
            context.lineTo(offset+(5*state.dimensions.cellSize), COORD_SIZE - offset)

            context.stroke();
            context.restore();

            //bottom-right corner
            context.save();
            context.beginPath();
            context.strokeStyle = 'rgb(0, 0, 0)';
            context.lineWidth = 6;
            context.moveTo(COORD_SIZE - offset, COORD_SIZE - offset);
            context.lineTo(COORD_SIZE - offset - (5*state.dimensions.cellSize), COORD_SIZE - offset)
            context.moveTo(COORD_SIZE - offset, COORD_SIZE - offset);
            context.lineTo(COORD_SIZE - offset, COORD_SIZE -offset - (5*state.dimensions.cellSize))

            context.stroke();
            context.restore();

            // Here is where I render the turret being dragged if necessary
            if(state.mouseData.turret !== null){
                context.save();
                context.globalAlpha = .5;

                if (turret_platform.isReady) { 
                    context.drawImage(turret_platform, state.mouseData.pos.x - state.dimensions.cellSize/2, state.mouseData.pos.y - state.dimensions.cellSize/2, state.dimensions.cellSize, state.dimensions.cellSize);
                }

                if (state.mouseData.turret.image.isReady) {
                    // I need to rotate the turret based on its theta
                    
                    context.drawImage(state.mouseData.turret.image, state.mouseData.pos.x - state.dimensions.cellSize/2, state.mouseData.pos.y - state.dimensions.cellSize/2, state.dimensions.cellSize, state.dimensions.cellSize);
                }
                context.restore();

                // draw the range circle
                // context.arc(x,y,r,sAngle,eAngle,counterclockwise);
                context.save();
                context.beginPath();
                context.arc(state.mouseData.pos.x, state.mouseData.pos.y, state.mouseData.turret.range, 0, 2 * Math.PI);
    
                context.lineWidth = 2;
                context.strokeStyle = 'rgb(0, 0, 255)';
    
                context.stroke(); 
                context.closePath();
                context.restore();

            }

            state.spiderManager.render(context);
            state.particleManager.render(context);
            
            if(state.endGame == true){
                context.save();
                context.strokeStyle = 'rgb(255,0,0)';
                context.lineWidth = 6;
                context.font = "48px Didot, serif";
                context.fillStyle = 'rgb(255,0,0)';
                context.textAlign = "center";
        
                context.fillText("GAME OVER " + state.endGameCount.toString(), COORD_SIZE/2, COORD_SIZE/2); 
                context.restore();
            }
        }
    }
    // Now all update methods
    return {render}
}(TD.gameState));