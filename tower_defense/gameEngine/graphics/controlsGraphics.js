Graphics.controls = (function(state){

    let COORD_SIZE = 1000

    let background = new Image();
    
    background.isReady = false;

    background.onload = function() {
        // console.log("Image was loaded")
        this.isReady = true;
    };
    
    background.src = "gameEngine/images/controls_background.jpg";

    function render(context){
        // The masterUpdate could act as the navigator checking the state for a current screen to update
        // updating events that navigate between screens need to appropriately remove current event listeners

        // I need to apply a background.
        context.save();

        if (background.isReady) {
            context.drawImage(background, 0, 0, COORD_SIZE , COORD_SIZE);
            // console.log("Drew background")
        }

        context.restore();

        // This is the band for the menu title to be on.
        context.save();
        context.fillStyle = "rgb(0,0,0)";
        context.fillRect(0, COORD_SIZE/4+20, COORD_SIZE, 120);

        context.strokeStyle = "rgb(255,255,255)";
        context.strokeRect(0, COORD_SIZE/4+20, COORD_SIZE, 120);

        context.restore();

        // This is the title render
        context.save();
    
        context.strokeStyle = 'rgb(255, 255, 255)';
        context.lineWidth = 6;
    
        context.font = "italic 50px Didot, serif";
        context.fillStyle = 'rgb(255, 255, 255)';
        context.textAlign = "center";
    
        context.fillText(state.title, COORD_SIZE/2, COORD_SIZE/4+80); 

        context.font = "30px Didot, serif";
        context.fillText(state.subTitle, COORD_SIZE/2, COORD_SIZE/4+120); 

        context.restore();

    
        // Create all the buttons using the menu object in the eventList
        for(let i = 0; i < state.buttons.length; i++){
            // console.log(state.buttons)
            if(state.listening && state.changingButtonIndex == i){
                context.save();
                context.fillStyle = "rgb(0,0,0)";
                context.fillRect(state.buttons[i].x, state.buttons[i].y, state.buttons[i].width, state.buttons[i].height);
        
                context.strokeStyle = "rgb(0,255,0)";
                context.strokeRect(state.buttons[i].x, state.buttons[i].y, state.buttons[i].width, state.buttons[i].height);
                context.restore();
    
                context.strokeStyle = "rgb(0,255,0)";
                context.strokeRect(state.buttons[i].x, state.buttons[i].y, state.buttons[i].width-50, state.buttons[i].height);
                context.restore();
        
                context.save();
                context.strokeStyle = 'rgb(0, 255, 0)';
                context.lineWidth = 6;
            }
            else{
                context.save();
                context.fillStyle = "rgb(0,0,0)";
                context.fillRect(state.buttons[i].x, state.buttons[i].y, state.buttons[i].width, state.buttons[i].height);
        
                context.strokeStyle = "rgb(255,255,255)";
                context.strokeRect(state.buttons[i].x, state.buttons[i].y, state.buttons[i].width, state.buttons[i].height);
                context.restore();

                context.strokeStyle = "rgb(255,255,255)";
                context.strokeRect(state.buttons[i].x, state.buttons[i].y, state.buttons[i].width-50, state.buttons[i].height);
                context.restore();
        
                context.save();
                context.strokeStyle = 'rgb(255, 255, 255)';
                context.lineWidth = 6;
            }
            
        
            context.font = "16px Sans-Serif";
            context.fillStyle = 'rgb(255, 255, 255)';
            context.textAlign = "center";
        
            context.fillText(state.buttons[i].name, state.buttons[i].x+75, state.buttons[i].y+25);
            context.fillText(state.buttons[i].key, state.buttons[i].x+175, state.buttons[i].y+25);

            context.restore(); 
        }

        // Render the menu navigation button
        context.save();
        context.fillStyle = "rgb(0,0,0)";
        context.fillRect(state.navMenuButton.x, state.navMenuButton.y, state.navMenuButton.width, state.navMenuButton.height);

        context.strokeStyle = "rgb(255,255,255)";
        context.strokeRect(state.navMenuButton.x, state.navMenuButton.y, state.navMenuButton.width, state.navMenuButton.height);
        context.restore();

        context.save();
        context.strokeStyle = 'rgb(255, 255, 255)';
        context.lineWidth = 6;
    
        context.font = "16px Sans-Serif";
        context.fillStyle = 'rgb(255, 255, 255)';
        context.textAlign = "center";
    
        context.fillText(state.navMenuButton.name, state.navMenuButton.x+100, state.navMenuButton.y+25);

        context.restore(); 
        
    }
    return {render}

}(TD.controlState));
