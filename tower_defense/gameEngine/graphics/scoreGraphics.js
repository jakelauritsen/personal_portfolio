Graphics.scores = (function(state){
    // This must be the first thing in hte update method to preserve the processInput -> update -> render, model
    // processInput();

    // let state = LunarLanding.state;
    let COORD_SIZE = 1000

    // how to load background
    let background = new Image();
    background.isReady = false;

    background.onload = function() {
        console.log("Image was loaded")
        this.isReady = true;
    };
    
    background.src = "gameEngine/images/controls_background.jpg";

    function render(context){
        // Render the background
        if (background.isReady) {
            context.drawImage(background, 0, 0, COORD_SIZE , COORD_SIZE);
        }

        // This is the band for the menu title to be on.
        context.save();
        context.fillStyle = "rgb(0,0,0)";
        context.fillRect(0, COORD_SIZE/4+20, COORD_SIZE, 80);
        context.strokeRect(0, COORD_SIZE/4+20, COORD_SIZE, 80);

        context.restore();

        // This is the title render
        context.save();
    
        context.strokeStyle = 'rgb(255, 255, 255)';
        context.lineWidth = 6;
    
        context.font = "italic 50px Didot, serif";
        context.fillStyle = 'rgb(255, 255, 255)';
        context.textAlign = "center";
    
        context.fillText(state.title, COORD_SIZE/2, COORD_SIZE/4+80); 
    
        context.restore();

        // Score rendering 
        let offset = 30;
        for(let i = 0; i < state.highScores.length; i++){
            context.save();
        
            context.strokeStyle = 'rgb(255, 255, 255)';
            context.lineWidth = 6;
            
            context.font = "30px Didot, serif";
            context.fillStyle = 'rgb(255, 255, 255)';
            context.textAlign = "center";
            
            tmpStr = (i+1).toString() + ': ' + state.highScores[i].toString();
            context.fillText(tmpStr, COORD_SIZE/2, COORD_SIZE/4+120+(i*offset)+20); 
        
            context.restore();
        }

        // Render the return to menu button, then handle the navigation
        context.save();
        context.fillStyle = "rgb(0,0,0)";
        context.fillRect(state.menuButton.x, state.menuButton.y, state.menuButton.width, state.menuButton.height);

        context.strokeStyle = "rgb(255,255,255)";
        context.strokeRect(state.menuButton.x, state.menuButton.y, state.menuButton.width, state.menuButton.height);
        context.restore();

        context.save();
        context.strokeStyle = 'rgb(255, 255, 255)';
        context.lineWidth = 6;
    
        context.font = "16px Sans-Serif";
        context.fillStyle = 'rgb(255, 255, 255)';
        context.textAlign = "center";
    
        context.fillText(state.menuButton.name, state.menuButton.x+75, state.menuButton.y+25);
        context.restore(); 

    }
    // Now all update methods
    return {render}
}(TD.scoreState));