Components.gameObjects.soundLoader = (function(){
    function loadSound(source) {
        let sound = new Audio();
        sound.src = source;
        return sound;
    }

    function loadAudio() {
        Components.gameObjects.sounds = {}
        // Reference: https://freesound.org/data/previews/156/156031_2703579-lq.mp3

        // the loadSound function simply adds the eventListeners to the audio object, which just print statuses, I think.
        
        Components.gameObjects.sounds['menuMusic'] = loadSound('gameEngine/sound_music_fx/menu_music.mp3');
        Components.gameObjects.sounds['menuMusic'].loop=true;

        Components.gameObjects.sounds['gamePlayMusic'] = loadSound('gameEngine/sound_music_fx/gameplay_music.mp3');
        Components.gameObjects.sounds['gamePlayMusic'].loop=true;
        Components.gameObjects.sounds['gamePlayMusic'].volume=.8;

        Components.gameObjects.sounds['splat'] = loadSound('gameEngine/sound_music_fx/splat.mp3');

        Components.gameObjects.sounds['pelletShooter'] = loadSound('gameEngine/sound_music_fx/pellet_gun_sfx.mp3');

        // Components.gameObjects.sounds['splat'].volume=.5;

        // TD.gameObjects.sounds['button'] = loadSound('gameEngine/sound_music_fx/button_sfx.mp3');

        // TD.gameObjects.sounds['thruster'] = loadSound('gameEngine/sound_music_fx/shorter_thruster_sfx0001.mp3');
        // TD.gameObjects.sounds['thruster'].volume=.1;
        // // Reference: https://freesound.org//data/previews/109/109662_945474-lq.mp3
        // LunarLanding.sounds['audio/sound-2'] = loadSound('audio/sound-2.mp3', 'Sound 2', 'id-play2');
        // // Reference: https://www.bensound.com/royalty-free-music/track/extreme-action
        // LunarLanding.sounds['audio/bensound-extremeaction'] = loadSound('audio/bensound-extremeaction.mp3', 'Music', 'id-play3');
    }

    loadAudio();


}());

