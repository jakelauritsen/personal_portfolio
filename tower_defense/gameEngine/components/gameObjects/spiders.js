// I need the game state in here for updating according to the path
Components.gameObjects.spiders = (function(state){
    let COORD_SIZE = 1000;
    let spiders = [];
    let spiderTime = 0;
    let finishedSpiders = [];
    floatingNums = [];

    
    // load animation sheet
    let spiderSheet = new Image(2080,520);
    spiderSheet.isReady = false;

    spiderSheet.onload = function() {
        this.isReady = true;
    };

    spiderSheet.src = "gameEngine/images/spiders_sprite_sheet.png";

    // the path passed to pathBuilder is a list of cells 
    // I need it to make a list of coord to work towards, specifically the center of each cell
    function pathBuilder(path, xStart, yStart, xEnd, yEnd){
        let newPath = []
        newPath.push({x:xStart,y:yStart})
        for(let cell = 0; cell < path.length; cell++){
            let cx = path[cell].box.x + path[cell].box.width/2-65/2
            let cy = path[cell].box.y + path[cell].box.height/2-65/2
            newPath.push({x:cx,y:cy})
        }
        newPath.push({x:xEnd,y:yEnd})
        // I need to add the end position to leave the grid
        // console.log(newPath)
        return newPath;
    }

    function coordsToGraphNode(coords, board, isInTester){
        // iterate through the board cells and find the cell the coord lies in, convert row and col to ID and return ID

        for(let r = 0; r < board.length; r++){
            for(let c = 0; c < board.length; c++){
                if(coords.x == board[r][c].box.x){
                    if(coords.y == board[r][c].box.y){

                        return r*board.length+c
                    }
                }
            }
        }
    }

    function graphNodesToCoords(path, board){
        path.splice(path.length-1,1)
        newPath = []
        for(let i = 0; i < path.length; i++){
            let row = Math.floor(path[i]/board.length);
            let col = path[i]%board.length
            // console.log(path.length)

            newPath.push(board[row][col])
        }
        return newPath;
    }

    function reInitAllSpiderPaths(pathFinder, board, isInTester, endSpawnID){
        // Spiders between the last cell and the despawn point will not be redirected
        for(let i = 0; i < spiders.length; i++){
            if(spiders[i].path.length > 2){

                // src, dest, board
                // src and dest need to be relevant graph id's mapped from the spiders 
                let coords = {x:spiders[i].path[1].x,y:spiders[i].path[1].y}

                startGraphID = coordsToGraphNode(coords, board, isInTester);
                
                path = pathFinder.calcShortestPath(startGraphID, endSpawnID, board);

                if(path === undefined){
                    return false;
                }

                path = graphNodesToCoords(path, board);

                spiders[i].path = pathBuilder(path, spiders[i].pos.x, spiders[i].pos.y, spiders[i].endPos.x, spiders[i].endPos.y)
            }
        }
        return true
    }

    function spiderNearCell(cell){
        for(let i = 0; i < spiders.length; i++){
            // check each spider to see if the first or second elements in their path are the same as the cell passed in 
            if(spiders[i].path.length > 2){
                if(Math.abs(spiders[i].path[0].x - (cell.box.x + cell.box.width/2-65/2)) < .01){
                    if(Math.abs(spiders[i].path[0].y - (cell.box.y + cell.box.height/2-65/2)) < .01){
                        return true;
                    }
                }
                if(Math.abs(spiders[i].path[1].x - (cell.box.x + cell.box.width/2-65/2)) < .01){
                    if(Math.abs(spiders[i].path[1].y - (cell.box.y + cell.box.height/2-65/2)) < .01){
                        return true;
                    }
                }
            }
        }
        return false
    }

    // returns a collision data object with a boolean and a coord
    function collidesWithSpiders(circle){
        // This idea is to calculate the center of the circle given and calculate if it overlaps the circle for each spider
        for(let i = 0; i < spiders.length; i++){
            dist = Math.sqrt(Math.pow((circle.x-(spiders[i].pos.x+65/2)),2)+Math.pow((circle.y-(spiders[i].pos.y+65/2)),2))
            //This is how I'm calculating each spiders rendered circle
            //spiders[i].pos.x+65/2, spiders[i].pos.y+65/2, 10, 0, 2 * Math.PI
            if( circle.r + spiders[i].collRad > dist){
                return {collision:true,target:{x:spiders[i].pos.x+65/2,y:spiders[i].pos.y+65/2}, spider:spiders[i]};
            }
        }
        return {collision:false,target:null,spider:null}
    }

    function addSpiders(count, xStart, yStart, xEnd, yEnd, path){
        spider = {
            recAnimTime: 0,
            recMoveTime:0,
            // orientation can be 0, 2, 4, 6
            orientation:4,
            animFrame:0,
            health:100,
            killValue:10,
            collRad:10,
            endPos:{x:xEnd-65/2, y:yEnd-65/2},
            path:pathBuilder(path, xStart-65/2, yStart-65/2, xEnd-65/2, yEnd-65/2),
            pos:{
                x:xStart-65/2,
                y:yStart-65/2,
            },
            dim:{
                width:65,
                height:65,
            },
        }
        for(let i = 0; i < count; i++){
            spiders.push(spider)
        }
    }

    function crossProd(v1,v2){
        return (v1.x*v2.y)-(v1.y*v2.x);
    }

    function computeAngle(theta, cellCent, targCent){
        let v1 = {
            x:Math.cos(theta),
            y:Math.sin(theta)
        }
        let v2 = {
            x: targCent.x - cellCent.x,
            y: targCent.y - cellCent.y,
        }
        let dp =null;
        let angle = null;

        v2.len = Math.sqrt(v2.x*v2.x+v2.y*v2.y)
        v2.x/=v2.len
        v2.y/=v2.len

        dp = v1.x*v2.x + v1.y*v2.y;
        angle = Math.acos(dp)

        let cp = crossProd(v1,v2);
        
        return {angle:angle, cp:cp}
    }

    function getFrameFromAngle(theta, cp){
        if(cp < 0){
            // if the angle was negative rotate 2 pi to make it pos
            theta = -1 * theta + 2*Math.PI
        }

        if(theta < Math.PI/2){
            return 4;
        } else if(theta < Math.PI){
            return 6;
        }else if(theta < 3*Math.PI/2){
            return 0;
        }else {
            return 2;
        }

    }

    function nextPos(x0,y0,x1,y1,x2,y2,unit,spider){
        // given where spider started, is, and headed calculate next position.
        dist = Math.sqrt(Math.pow((x2-x0),2)+Math.pow((y2-y0),2))
        // console.log(dist)
        x3 = x1 + (unit/dist)*(x2-x0);
        y3 = y1 + (unit/dist)*(y2-y0);

        overhead = Math.sqrt(Math.pow((x3-x0),2)+Math.pow((y3-y0),2))

        if (overhead > dist){
            // this means we would pass our target
            // I need to return the coord where we travel to the goal then remaining dist to next goal
            distLeft = overhead - dist;

            // now move dist Left towards new goal
            if (spider.path.length > 2){
                spider.path.shift()
                // console.log(spider.path)
                let x4 = spider.path[1].x
                let y4 = spider.path[1].y,
                newDist = Math.sqrt(Math.pow((x4-x2),2)+Math.pow((y4-y2),2))
                x5 = x2 + distLeft/newDist*(x4-x2);
                y5 = y2 + distLeft/newDist*(y4-y2);

                // here is also a good spot to check/change orientation
                result = computeAngle(0,{x:x2,y:y2},{x:x4,y:y4})
                spider.orientation = getFrameFromAngle(result.angle, result.cp);

                return {x:x5,y:y5}
            }
            else{
                // if there are only 2 path points left then we just set to last point as to not travel past the end
                return {x:spider.path[1].x,y:spider.path[1].y}
            }
            // pop first element 
        }
        else{
            // still haven't passed target
            return {x:x3,y:y3}
        }
    }

    // This is a helper function for the render method to set next frame based the specific spider state
    function nextFrame(spider){
        // look at the attributes of the spider and the state and set next frame
        // the spider sheet is 13 clips long
        return 2 + ((spider.animFrame + 1)%7) // this will produce values [0,12]
    }
    
    function endGame(){
        state.endGame = true;
        state.endGameTimer = 0;
        state.endGameCount = 3;
    }

    function cleanSpiders(){
        spiders = [];
        spiderTime = 0;
        finishedSpiders = [];
        floatingNums = [];
    }

    function killedSpider(x,y){
        floatNum = {
            fontSize:12,
            val:100,
            pos:{x,y},
            lifeSpan:1000,
            timer:0,
            opacVal:1,
        }
        floatingNums.push(floatNum);
        Components.gameObjects.sounds['splat'].play();


    }

    function updateSpiderAnimation(elapsedTime){
        // after x amount of time change the animation value on each spider

        for(let i = 0; i < spiders.length; i++){
            if (spiders[i].health > 0){

                if(spiders[i].recAnimTime + elapsedTime <= 150){
                    spiders[i].recAnimTime += elapsedTime;
                }
                else{
                    spiders[i].recAnimTime += elapsedTime;
                    spiders[i].recAnimTime -= 150;
                    spiders[i].animFrame = nextFrame(spiders[i])
                }

                if(spiders[i].recMoveTime + elapsedTime <= 50){
                    spiders[i].recMoveTime += elapsedTime;
                }
                else{
                    spiders[i].recMoveTime += elapsedTime;
                    spiders[i].recMoveTime -= 50;
                    // move spider here
                    // I need to take a step towards the next goal, if I will pass it then set to goal, plus march the remainding distance towards the goal after that
                    
                    newPos = nextPos(spiders[i].path[0].x, spiders[i].path[0].y, spiders[i].pos.x, spiders[i].pos.y, spiders[i].path[1].x, spiders[i].path[1].y, 3, spiders[i])
                    if(newPos.x == spiders[i].endPos.x && newPos.y == spiders[i].endPos.y){
                        spiders.splice(i,1)
                        state.stats.lives -=1;
                        if (state.stats.lives == 0){
                            endGame();
                        }
                        // I need to add the lucky spiders the the finished spiders list so they can later be accounted for scoring purposes
                    }
                    else{
                        spiders[i].pos.x = newPos.x;
                        spiders[i].pos.y = newPos.y;
                    }
                }
            }
            else{
                state.stats.score+=100;
                state.stats.cash+=200;
                killedSpider(spiders[i].pos.x+65/2, spiders[i].pos.y+65/2);
                spiders.splice(i,1);
            }
        }
    }



    function render(context){
        // depending on animation frame value in each spider render the spider at its position
        for(let i = 0; i < spiders.length; i++){
            if (spiderSheet.isReady) {
                // console.log("rendering spiderz")
                // void ctx.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);
                // sheet is 8x32 sprites
                // sheet dimensions are 4086x1024
                // the idea for sub images is to clip natural size of photo then place image
                context.drawImage(spiderSheet, 4086/32*spiders[i].animFrame, 1024/8*spiders[i].orientation+20 ,4086/32,1024/8, spiders[i].pos.x, spiders[i].pos.y, spiders[i].dim.width, spiders[i].dim.height);
            }
            // context.strokeStyle = "rgb(255,255,255,.6)";
            // context.strokeRect(60, 60, 65, 65);

            // This will render a circle around the spider for collision detection purposes
            // I will just calculate the collision circle when I need it, or it will need to be updated with each position update
            let spiderCentX = spiders[i].pos.x+65/2,
                spiderCentY = spiders[i].pos.y+65/2;
            
            // context.save();
            // context.beginPath();
            // context.arc(spiderCentX, spiderCentY, spiders[i].collRad, 0, 2 * Math.PI);

            // context.lineWidth = 2;
            // context.strokeStyle = 'rgb(255, 0, 0)';

            // context.stroke(); 
            // context.closePath();
            // context.restore();

            // This is the health bar
            // renders a rectangle shifted up and to the left of the current spiders location. 
            context.save();
            context.strokeStyle = "rgb(255,255,255)";
            let r = Math.floor(Math.abs(spiders[i].health*2.55-255));
            let g = Math.floor(spiders[i].health*2.55);
            let barWidth = Math.round(spiders[i].health*.15)
            let shift = 10;
            context.fillStyle = "rgb(0,0,0)";
            context.fillRect(spiderCentX+shift, spiderCentY-shift, 15, 5);
            context.fillStyle = "rgb(" + r.toString() + "," + g.toString() + ",0)";
            context.fillRect(spiderCentX+shift, spiderCentY-shift, barWidth, 5);
            context.strokeRect(spiderCentX+shift, spiderCentY-shift, 15, 5);
            context.restore();
            
        }

        for(let i = 0; i < floatingNums.length; i++){
            context.save();
            context.strokeStyle = 'rgb(255,255,255,' + floatingNums[i].opacVal.toString() +')';
            context.lineWidth = 6;
            context.font =  floatingNums[i].fontSize.toString() + "px Didot, serif";
            context.fillStyle = 'rgb(255,255,255,' + floatingNums[i].opacVal.toString() +')';
            context.textAlign = "center";
    
            context.fillText(floatingNums[i].val.toString(), floatingNums[i].pos.x, floatingNums[i].pos.y); 
            context.restore();
        }    
    }

    function updateFloatingNums(elapsedTime){
        unexpiredNums = []
        for(let i = 0; i < floatingNums.length; i++){
            if(floatingNums[i].lifeSpan - elapsedTime > 0){
                if(floatingNums[i].timer + elapsedTime <= 100){
                    floatingNums[i].timer += elapsedTime;
                }
                else{
                    floatingNums[i].timer += elapsedTime;
                    floatingNums[i].timer -= 100;

                    floatingNums[i].pos.y -= 2;
                    floatingNums[i].fontSize += 1;
                    floatingNums[i].opacVal -= .05;
                }
                floatingNums[i].lifeSpan -= elapsedTime;
                unexpiredNums.push(floatingNums[i]);
            }
        }
        floatingNums = unexpiredNums;
    }

    function update(elapsedTime){
        // will move spiders until dead across path changing orientation
        updateSpiderAnimation(elapsedTime);
        updateFloatingNums(elapsedTime);

        if(spiders.length == 0){
            state.isWaveOn = false;
        }
    }

    return {
        addSpiders,
        update,
        render,
        collidesWithSpiders,
        spiderNearCell,
        reInitAllSpiderPaths,
        cleanSpiders,
    }
}(TD.gameState));
