// I devoted 12 hours straight to trying to implement my own shortest path, but i failed. Heres is one that works that I heavily refered to.
// https://stackoverflow.com/questions/32527026/shortest-path-in-javascript

Components.utilities.shortestPathSolver = function() {
    let that = {};
    let graph = null;
    // add functions to that.

    function checkForOpenVert(i, board, BS){
        let r = Math.floor(i/BS);
        let c = i % BS;

        if(board[r][c].turret == null){
            return true;
        }
    }

    function buildGraph(BS, graph, board){
        // adds all board graph nodes
        for(let i = 0; i < BS*BS; i++){
            
            // if i is not a cell with a turret on it 
            if(checkForOpenVert(i, board, BS)){

                // if not last row on board
                if(i < (BS*BS-BS)){
                    // last col
                    if(i % BS == BS-1){
                        // only check to add a bottom vert
                        if (checkForOpenVert(i+BS, board, BS)){
                            graph.addEdge(i, i+BS)
                        }
                    }
                    // check to add both bottom and right vert
                    else{
                        if (checkForOpenVert(i+1, board, BS)){
                            graph.addEdge(i, i+1)                            
                        }
                        if (checkForOpenVert(i+BS, board, BS)){
                            graph.addEdge(i,i+BS)
                        }
                    }
                }
                // only check to add right vert
                else{
                    // last cell will not add any edges to the graph
                    if (i != BS*BS-1){
                        if (checkForOpenVert(i+1, board, BS)){
                            graph.addEdge(i, i+1)                            
                        }
                    }
                }
            }
        }

        // adds spawn point nodes
        if (checkForOpenVert(5, board, BS)){
            graph.addEdge(BS*BS,5);                   
        }
        if (checkForOpenVert(6, board, BS)){
            graph.addEdge(BS*BS,6);                   
        }
        if (checkForOpenVert(60, board, BS)){
            graph.addEdge(BS*BS+1,60);
        }
        if (checkForOpenVert(72, board, BS)){
            graph.addEdge(BS*BS+1,72);                   
        }
        if (checkForOpenVert(71, board, BS)){
            graph.addEdge(BS*BS+2,71);                   
        }
        if (checkForOpenVert(83, board, BS)){
            graph.addEdge(BS*BS+2,83);                   
        }
        if (checkForOpenVert(138, board, BS)){
            graph.addEdge(BS*BS+3,138);                   
        }
        if (checkForOpenVert(138, board, BS)){
            graph.addEdge(BS*BS+3,137);                   
        }

    }


    function Graph() {
        var neighbors = this.neighbors = {}; // Key = vertex, value = array of neighbors.
      
        this.addEdge = function (u, v) {
          if (neighbors[u] === undefined) {  // Add the edge u -> v.
            neighbors[u] = [];
          }
          neighbors[u].push(v);
          if (neighbors[v] === undefined) {  // Also add the edge v -> u in order
            neighbors[v] = [];               // to implement an undirected graph.
          }                                  // For a directed graph, delete
          neighbors[v].push(u);              // these four lines.
        };
      
        return this;
      }
      
      function bfs(graph, source) {
        var queue = [ { vertex: source, count: 0 } ],
            visited = { source: true },
            tail = 0;
        while (tail < queue.length) {
          var u = queue[tail].vertex,
              count = queue[tail++].count;  // Pop a vertex off the queue.
          print('distance from ' + source + ' to ' + u + ': ' + count);
          graph.neighbors[u].forEach(function (v) {
            if (!visited[v]) {
              visited[v] = true;
              queue.push({ vertex: v, count: count + 1 });
            }
          });
        }
      }
      
      function shortestPath(graph, source, target) {
        if (source == target) {   // Delete these four lines if
          return;                 // when the source is equal to
        }                         // the target.
        var queue = [ source ],
            visited = { source: true },
            predecessor = {},
            tail = 0;
        while (tail < queue.length) {
          var u = queue[tail++],  // Pop a vertex off the queue.
              neighbors = graph.neighbors[u];
          for (var i = 0; i < neighbors.length; ++i) {
            var v = neighbors[i];
            if (visited[v]) {
              continue;
            }
            visited[v] = true;
            if (v === target) {   // Check if the path is complete.
              var path = [ v ];   // If so, backtrack through the path.
              while (u !== source) {
                path.push(u);
                u = predecessor[u];
              }
              path.push(u);
              path.reverse();
              return path;
            }
            predecessor[v] = u;
            queue.push(v);
          }
        }
      }


    // using the boards state builds a graph for the shortest path alg to reference
    that.setGraph = function(board){
        graph = new Graph();
        let BS = board.length
        buildGraph(BS, graph, board)
    }
      
    that.calcShortestPath = function (src,dest,board) {

        // if a graph has not been built yet build one otherwise just use the last built graph
        if(graph === null){
            graph = new Graph();
            let BS = board.length
            buildGraph(BS, graph, board)
        }

        let path = shortestPath(graph, src, dest);
        return path;
    };

    return that;
};
