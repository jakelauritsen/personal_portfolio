Components.utilities.particleSystem = (function(){
    // pellet style shooter particles 
    let turretType1Ps = {timer:0,particles:[]}
    // cannon style shooter particles 
    let turretType2Ps = []
    // missile style shooter particles 
    let turretType3Ps = []
    // lazer style particles
    let turretType4Ps = []


    // This function will be added as a property to type 1 turrets.
    function addType1Particles(elapsedTime, turret, target, centerX, centerY, spider){

        if(turretType4Ps.timer + elapsedTime <= 100){
            turretType4Ps.timer += elapsedTime;
        }
        else{
            turretType4Ps.timer += elapsedTime;
            turretType4Ps.timer -= 100;

            //the lifespan of the particles needs to be a function of time and target
            dist = Math.sqrt(Math.pow((target.x-centerX),2)+Math.pow((target.y-centerY),2))


            // change after x time goes here
            // these offset will need to be added to the centerX, centerY when I can actually render them and callibrate these constants
            let x_offset =  10 * Math.cos(turret.theta)
            let y_offset =  10 * Math.sin(turret.theta)
            p = {
                power:turret.tier,
                target: spider,
                theta: turret.theta,
                pos: {x:centerX + x_offset,y:centerY+y_offset},
                lifeSpan:dist,
                pelletRadius:3,
            }
            turretType1Ps.particles.push(p);
        }
    }

    function addType2Particles(elapsedTime){

    }

    function addType3Particles(elapsedTime){

    }

    function addType4Particles(elapsedTime){

    }

    // These function will take the state of each particle in the globalish lists and update according to tyme and specific physics
    function updateType1Particles(elapsedTime){
        let unexpiredPs = []
        for(let i = 0; i < turretType1Ps.particles.length;i++){
            
            // deduct from lifespan, if lifespan is shorter than deduction remove, if not removed update position
            if(turretType1Ps.particles[i].lifeSpan - elapsedTime > 0){
                // console.log(turretType1Ps.particles[i].lifeSpan)
                turretType1Ps.particles[i].lifeSpan -= elapsedTime;
                // update position

                // coords.x += v * Math.cos(t) * elapsedTime/12
                // coords.y += -1 * v * Math.sin(t) * elapsedTime/12
                let v = 1
                turretType1Ps.particles[i].pos.x += v * Math.cos(turretType1Ps.particles[i].theta) * elapsedTime
                turretType1Ps.particles[i].pos.y += v * Math.sin(turretType1Ps.particles[i].theta) * elapsedTime

                unexpiredPs.push(turretType1Ps.particles[i]);
            }
            turretType1Ps.particles[i].target.health -= .1 * turretType1Ps.particles[i].power;
        }
        turretType1Ps.particles = unexpiredPs;
    }

    function updateType2Particles(elapsedTime){

    }

    function updateType3Particles(elapsedTime){

    }

    function updateType4Particles(elapsedTime){

    }

    function renderType1Particles(context){
        for(let i = 0; i < turretType1Ps.particles.length;i++){
            context.save();
            context.beginPath();
            context.arc(turretType1Ps.particles[i].pos.x, turretType1Ps.particles[i].pos.y, turretType1Ps.particles[i].pelletRadius, 0, 2 * Math.PI);

            context.lineWidth = 2;
            context.strokeStyle = 'rgb(0, 0, 0)';
            context.fillStyle = 'rgb(128, 128, 128)';

            context.fill();
            context.stroke(); 
            context.closePath();
            context.restore();
        }
    }
    function renderType2Particles(context){
        
    }
    function renderType3Particles(context){
        
    }
    function renderType4Particles(context){
        
    }


    function update(elapsedTime){
        updateType1Particles(elapsedTime);
        updateType2Particles(elapsedTime);
        updateType3Particles(elapsedTime);
        updateType4Particles(elapsedTime);
    }
    function render(context){
        renderType1Particles(context);
        renderType2Particles(context);
        renderType3Particles(context);
        renderType4Particles(context);
    }
    return{
        update,
        render,
        addType1Particles,
        addType2Particles,
        addType3Particles,
        addType4Particles,
    }
}());
