Drivers.update = (function(){

    // This is the initial screen to start the game with, thus menu
    TD.currentScreen = TD.menuScreen;


    function masterUpdate(elapsedTime){
        TD.currentScreen.update(elapsedTime);
        // The masterUpdate could act as the navigator checking the state for a current screen to update
        // updating events that navigate between screens need to appropriately remove current event listeners
    }
    // Now all update methods
    return {masterUpdate}
}());

