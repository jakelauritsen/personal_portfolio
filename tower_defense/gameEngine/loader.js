let TD = {
    // These are the states associated with each screen. 
    // To display a screen, the state of that screens is set to the currents screen in the master update
    currentScreen:{},
    testState:{},
    creditState:{},
    menuState:{},
    scoreState:{},
    gameState:{},
    controlState:{
        controls:[
            {name:"Upgrade Turret", key:"u"}, 
            {name:"Sell Turret", key:"s"}, 
            {name:"Start Next Level", key:"g"}
        ]
    },
    assets:{},  // This attribute:object is for the loader below 
};
let Graphics = {

};
let Drivers = {

};

// This is the model to hold functions such as controls, and particle effects, etc.
let Components = {
    input:{},
    utilities:{},
    gameObjects:{sounds:{},},
}


//------------------------------------------------------------------
//
// Purpose of this code is to bootstrap (maybe I should use that as the name)
// the rest of the application.  Only this file is specified in the index.html
// file, then the code in this file gets all the other code and assets
// loaded.
//
//------------------------------------------------------------------
TD.loader = (function() {
    'use strict';
    let scriptOrder = [{
            scripts: ['components/inputs/mouse'],
            message: 'Mouse component loaded',
            onComplete: null
        }, {
            scripts: ['components/inputs/keyboard'],
            message: 'Keyboard component loaded',
            onComplete: null
        }, {
            scripts: ['components/gameObjects/soundLoader'],
            message: 'Music and SFX loaded',
            onComplete: null
        }, {
            scripts: ['screens/menuScreen'],
            message: 'Menu Screen loaded',
            onComplete: null
        }, {
            scripts: ['drivers/menuDriver'],
            message: 'Driver for menu loaded',
            onComplete: null
        }, {
            scripts: ['graphics/menuGraphics'],
            message: 'Graphics for menu loaded',
            onComplete: null
        }, {
            scripts: ['screens/controlScreen'],
            message: 'Control Screen loaded',
            onComplete: null
        }, {
            scripts: ['drivers/controlsDriver'],
            message: 'Driver for controls loaded',
            onComplete: null
        }, {
            scripts: ['graphics/controlsGraphics'],
            message: 'Graphics for controls loaded',
            onComplete: null
        },  {
            scripts: ['screens/scoreScreen'],
            message: 'Score Screen loaded',
            onComplete: null
        }, {
            scripts: ['drivers/scoreDriver'],
            message: 'Driver for scores loaded',
            onComplete: null
        }, {
            scripts: ['graphics/scoreGraphics'],
            message: 'Graphics for scores loaded',
            onComplete: null
        }, {
            scripts: ['screens/creditsScreen'],
            message: 'Credits Screen loaded',
            onComplete: null
        }, {
            scripts: ['drivers/creditsDriver'],
            message: 'Driver for credits loaded',
            onComplete: null
        }, {
            scripts: ['graphics/creditsGraphics'],
            message: 'Graphics for credits loaded',
            onComplete: null
        }, {
            scripts: ['render'],
            message: 'Rendering loaded',
            onComplete: null
        }, {
            scripts: ['update'],
            message: 'Updating loaded',
            onComplete: null
        }, {
            scripts: ['gameLoop'],
            message: 'Core engine loaded',
            onComplete: null
        }, {
            scripts: ['components/utilities/shortestPath'],
            message: 'Path finder loaded',
            onComplete: null
        }, {
            scripts: ['components/gameObjects/spiders'],
            message: 'spiders loaded',
            onComplete: null
        }, {
            scripts: ['components/utilities/particleSystem'],
            message: 'spiders loaded',
            onComplete: null
        }, {
            scripts: ['screens/gameScreen'],
            message: 'Game Screen loaded',
            onComplete: null
        }, {
            scripts: ['drivers/gameDriver'],
            message: 'Driver for game loaded',
            onComplete: null
        }, {
            scripts: ['graphics/gameGraphics'],
            message: 'Graphics for game loaded',
            onComplete: null
        }];
        let assetOrder = [
        // {
        //     // Source: http://soundbible.com/2017-End-Fx.html
        //     // License: https://creativecommons.org/licenses/sampling+/1.0/
        //     key: 'effect-1',
        //     source: '/assets/End_Fx-Mike_Devils-724852498.mp3'
        // }, {
        //     // Source: http://soundbible.com/2044-Tick.html
        //     // License: https://creativecommons.org/licenses/by/3.0/us/
        //     key: 'effect-2',
        //     source: '/assets/Tick-DeepFrozenApps-397275646.mp3'
        // }, {
        //     // Source: http://soundbible.com/1705-Click2.html
        //     // License: https://creativecommons.org/licenses/by/3.0/us/
        //     key: 'effect-3',
        //     source: '/assets/Click2-Sebastian-759472264.mp3'
        // }
        ];

    //------------------------------------------------------------------
    //
    // Helper function used to load scripts in the order specified by the
    // 'scripts' parameter.  'scripts' expects an array of objects with
    // the following format...
    //    {
    //        scripts: [script1, script2, ...],
    //        message: 'Console message displayed after loading is complete',
    //        onComplete: function to call when loading is complete, may be null
    //    }
    //
    //------------------------------------------------------------------
    function loadScripts(scripts, onComplete) {
        //
        // When we run out of things to load, that is when we call onComplete.
        if (scripts.length > 0) {
            let entry = scripts[0];
            require(entry.scripts, function() {
                console.log(entry.message);
                if (entry.onComplete) {
                    entry.onComplete();
                }
                scripts.splice(0, 1);
                loadScripts(scripts, onComplete);
            });
        } else {
            onComplete();
        }
    }

    //------------------------------------------------------------------
    //
    // Helper function used to load assets in the order specified by the
    // 'assets' parameter.  'assets' expects an array of objects with
    // the following format...
    //    {
    //        key: 'asset-1',
    //        source: 'asset/url/asset.png'
    //    }
    //
    // onSuccess is invoked per asset as: onSuccess(key, asset)
    // onError is invoked per asset as: onError(error)
    // onComplete is invoked once per 'assets' array as: onComplete()
    //
    //------------------------------------------------------------------
    function loadAssets(assets, onSuccess, onError, onComplete) {
        let entry = 0;
        //
        // When we run out of things to load, that is when we call onComplete.
        if (assets.length > 0) {
            entry = assets[0];
            loadAsset(entry.source,
                function(asset) {
                    onSuccess(entry, asset);
                    assets.splice(0, 1);
                    loadAssets(assets, onSuccess, onError, onComplete);
                },
                function(error) {
                    onError(error);
                    assets.splice(0, 1);
                    loadAssets(assets, onSuccess, onError, onComplete);
                });
        } else {
            onComplete();
        }
    }

    //------------------------------------------------------------------
    //
    // This function is used to asynchronously load image and audio assets.
    // On success the asset is provided through the onSuccess callback.
    // Reference: http://www.html5rocks.com/en/tutorials/file/xhr2/
    //
    //------------------------------------------------------------------
    function loadAsset(source, onSuccess, onError) {
        let xhr = new XMLHttpRequest();
        let asset = null;
        let fileExtension = source.substr(source.lastIndexOf('.') + 1);    // Source: http://stackoverflow.com/questions/680929/how-to-extract-extension-from-filename-string-in-javascript

        if (fileExtension) {
            xhr.open('GET', source, true);
            xhr.responseType = 'blob';

            xhr.onload = function() {
                if (xhr.status === 200) {
                    if (fileExtension === 'png' || fileExtension === 'jpg') {
                        asset = new Image();
                    } else if (fileExtension === 'mp3') {
                        asset = new Audio();
                    } else {
                        if (onError) { onError('Unknown file extension: ' + fileExtension); }
                    }
                    asset.onload = function() {
                        window.URL.revokeObjectURL(asset.src);
                    };
                    asset.src = window.URL.createObjectURL(xhr.response);
                    if (onSuccess) { onSuccess(asset); }
                } else {
                    if (onError) { onError('Failed to retrieve: ' + source); }
                }
            };
        } else {
            if (onError) { onError('Unknown file extension: ' + fileExtension); }
        }

        xhr.send();
    }

    //------------------------------------------------------------------
    //
    // Called when all the scripts are loaded, it kicks off the demo app.
    //
    //------------------------------------------------------------------
    function mainComplete() {
        console.log('it is all loaded up');
        TD.core.initialize();
    }

    //
    // Start with loading the assets, then the scripts.
    console.log('Starting to dynamically load project assets');
    loadAssets(assetOrder,
        function(source, asset) {    // Store it on success
            TD.assets[source.key] = asset;
        },
        function(error) {
            console.log(error);
        },
        function() {
            console.log('All audio assets loaded');
            console.log('Starting to dynamically load project scripts');
            loadScripts(scriptOrder, mainComplete);
        }
    );

}());
