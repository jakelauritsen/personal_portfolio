Drivers.controls = (function(mouseInput, state){

    let handlers = [];
    // let mouseInput = Components.input.mouse();
    state.initialized = false;

    if(!window.localStorage.getItem('GrubTowerDefenseControls')){
        populateStorage();
    }
    else{
        setControls();
    }

    function populateStorage(){
        window.localStorage.setItem('GrubTowerDefenseControls', JSON.stringify(state.controls));
        console.log("Set up a local storage with " + JSON.stringify(state.controls))
    }

    function setControls(){
        state.controls = JSON.parse(window.localStorage.getItem('GrubTowerDefenseControls'));
        console.log("Found local storage: ")
        // console.log(state.controls)
    }


    function removerHandlers() {
        if (handlers.length > 0) {
            for (let i = 0; i < handlers.length; i++){
                mouseInput.unregisterHandler(handlers[i].type,handlers[i].id);
            }
        }
    }

    let clickHandler = function(evt, elapsedTime) {
        canvas = document.getElementById('canvas-main');
        context = canvas.getContext('2d');
        // console.log("made it in handler")
        var mousePos = mouseInput.getMousePos(canvas, evt);
    
        for(let i = 0; i < state.buttons.length; i++){
            if(mouseInput.isInside(mousePos, state.buttons[i])){
                state.listening = true;
                state.changingButtonIndex = i; 

                console.log("Clicked key binding button " + i.toString())
            }
        }

        if(mouseInput.isInside(mousePos, state.navMenuButton)){
            // change here needs to be general to all buttons
            // I need to change the current page and reinitialize appropriate states here, and unregister listeners 
            removerHandlers();
            window.removeEventListener("keyup", keyHandler);
            TD.currentScreen = TD.menuScreen;
            state.listening = false;
            state.changingButtonIndex = null;
            state.keyStrokes = []
            state.initialized = false;

            // console.log("Clicked nav to menu button");
        }        

    }

    // that.registerHandler = function(handler, type, requireCapture)
    // console.log(mouseInput)

    function keyHandler(e) {
        // console.log(`${e.key} : ${e.code}`);
        state.keyStrokes.push(e.key)
        // console.log(state.userInputs)
    }




    function processInput(){
        // console.log(state.keyStrokes.length)
        if (state.listening){
            if (state.keyStrokes.length > 0){
                console.log("Setting button " + state.changingButtonIndex.toString() + " to " + state.keyStrokes[0])
                state.controls[state.changingButtonIndex].key = state.keyStrokes[0]

                state.buttons = [];
                let button_offset = 0;
                for(let i = 0; i < state.controls.length; i++){
                    state.buttons.push({
                        name:state.controls[i].name,
                        key:state.controls[i].key,
                        x:(COORD_SIZE/2)-100,
                        y:(COORD_SIZE/2)-55 + button_offset,
                        width:200,
                        height:35,
                    });
                    button_offset+=45;
                }

                state.changingButtonIndex = null;
                state.listening = false;
                populateStorage();

            }
        }
        state.keyStrokes = [];
    }
    function initializeButtons(){
        state.buttons = [];
        let button_offset = 0;
        for(let i = 0; i < state.controls.length; i++){
            state.buttons.push({
                name:state.controls[i].name,
                key:state.controls[i].key,
                x:(COORD_SIZE/2)-100,
                y:(COORD_SIZE/2)-55 + button_offset,
                width:200,
                height:35,
            });
            button_offset+=45;
        }
    }

    function initialize(){
        window.addEventListener('keyup', keyHandler, false);
        handlers.push({id:mouseInput.registerHandler(clickHandler, 1, false), type:1});
        initializeButtons();
        state.initialized = true;
    }

    
    // I need to to implement a mouse handler for clicking the buttons.

    function update(elapsedTime){
        if(state.initialized === false){
            initialize();
        }
        processInput();
        mouseInput.update(elapsedTime);
    }
    // Now all update methods
    return {update}
}(Components.mouseInput, TD.controlState));
