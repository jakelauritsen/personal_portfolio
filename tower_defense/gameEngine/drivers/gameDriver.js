Drivers.game = (function(mouseInput, state){
    canvas = document.getElementById('canvas-main');
    context = canvas.getContext('2d');
    let COORD_SIZE = 1000
    let handlers = []


    function initState(){
        state.initialized = false;
        state.board = []
        state.spiderManager = Components.gameObjects.spiders;
        state.enemyPaths = []
        state.turretsOnBoard = []
        state.isWaveOn = false;
        state.currentSpawns=null;
        state.keyboard = Components.input.keyboard();
        state.particleManager = Components.utilities.particleSystem;
        state.endGame = false;
        state.waveTimer = 15000;
        state.spiderInterval = null;
        state.spiderIntervalTimer = 0;

        // data for rendering board and location finding for mouse operations
        state.dimensions = {
            boardSize:12,
            cellSize:65,
            offset: 110,
        }
        // Holds game data, like currency, score, level, and lives
        state.stats = {
            score:0,
            level:1,
            lives:10,
            cash:100000,
        }
        //
        state.store = {
            // references to the turrets, and all turret references start here.
            merch:[{
                turret:{
                    // the range for the turret when being dragged will be the radius for the collision detection and range itself
                    range:state.dimensions.cellSize/2+state.dimensions.cellSize,
                    theta:0,
                    turnTimer:0,
                    particleAdder: state.particleManager.addType1Particles,
                    src:{
                        one:"gameEngine/images/turrets/turret1Up1.png", 
                        two:"gameEngine/images/turrets/turret1Up2.png", 
                        three:"gameEngine/images/turrets/turret1Up3.png", 
                    }, 
                    price:1000, 
                    tier:1,
                }, 
                name:"Anti Crawler Turret",
                box:{}, 
            }, {
                turret:{
                    range:state.dimensions.cellSize/2+state.dimensions.cellSize*2,
                    theta:0,
                    turnTimer:0,
                    particleAdder: state.particleManager.addType2Particles,
                    src:{
                        one:"gameEngine/images/turrets/turret2Up1.png",
                        two:"gameEngine/images/turrets/turret2Up2.png",
                        three:"gameEngine/images/turrets/turret2Up3.png",
                    }, 
                    price:3000, 
                    tier:1,
                }, name:"Explosive Anti Crawler Turret", box:{}, 
            }, {
                turret:{
                    range:state.dimensions.cellSize/2+state.dimensions.cellSize*1.5,
                    theta:0,
                    turnTimer:0,
                    particleAdder: state.particleManager.addType3Particles,
                    src:{
                        one:"gameEngine/images/turrets/turret3Up1.png",
                        two:"gameEngine/images/turrets/turret3Up2.png",
                        three:"gameEngine/images/turrets/turret3Up3.png",
                    }, 
                    price:2000, 
                    tier:1,
                }, 
                name:"Anti Swarming Turret", 
                box:{}, 
            }, {
                turret:{
                    range:state.dimensions.cellSize/2+state.dimensions.cellSize*4,
                    theta:0,
                    turnTimer:0,
                    particleAdder: state.particleManager.addType4Particles,
                    src:{
                        one:"gameEngine/images/turrets/turret4Up1.png",
                        two:"gameEngine/images/turrets/turret4Up2.png",
                        three:"gameEngine/images/turrets/turret4Up3.png", 
                    },
                    price:5000, 
                    tier:1,
                }, 
                name:"Anti GRUB Turret", 
                box:{}, 
            }],
    
            features:[{
                name:"Upgrade",
                box:{}
            },
            {
                name:"Sale",
                box:{}
            },
            {
                name:"Unselect",
                box:{}
            },
            ]
        }
        // A lot of the features of this game depend on a selected turret/cell its in
        // This is thus a reference to the cell which contains the turret. This will provide both without searching.
        state.selectedCells = [];
    
        state.mouseData = {
            down:false,
            downIn:null,
            up:false,
            pos:null,
            turret:null,
        }

    }
    
    initState();


    function removerHandlers() {
        if (handlers.length > 0) {
            for (let i = 0; i < handlers.length; i++){
                mouseInput.unregisterHandler(handlers[i].type,handlers[i].id);
            }
        }
    }

    function userMouseOn(evt){
        let mousePos = mouseInput.getMousePos(canvas, evt);
        // console.log(mousePos)

        // mouseInput.isInside(pos, rect) | rect is a {x,y,w,h}
        // thus I need to have state boxes available. Boxes are built when this script is initialized.
        // I was going to return a reference to the object that the mouse was clicked in but with all the different cases this might not be that helpful
        // so I return a number as a representative of what was found. Nvmnd I like the references for the case of cells so I will do an object with both

        // I need to check store.merch boxes
        for(let i = 0; i < state.store.merch.length; i++){
            if(mouseInput.isInside(mousePos, state.store.merch[i].box)){
                return {id:0,ref:state.store.merch[i]};
            }
        }
        // I need to check store.sellBox and store.upBox
        for(let i = 0; i < state.store.features.length; i++){
            if(mouseInput.isInside(mousePos, state.store.features[i].box)){
                // console.log(state.store.features[i].name)
                return {id:1,ref:state.store.features[i]};
            }
        }
        // I then will check the board as a whole, and finally each cell to reduce the times worst case searching happens
        if(mouseInput.isInside(mousePos, {x:state.dimensions.offset,y:state.dimensions.offset,width:state.dimensions.cellSize*state.dimensions.boardSize,height:state.dimensions.cellSize*state.dimensions.boardSize})){
            // now check each cell
            // console.log("In board")

            for(let row = 0; row < state.board.length; row++){
                for(let col = 0; col < state.board[0].length; col++){
                    // cells in the board need a x y attribute for this generic isInside alg to work
                    if(mouseInput.isInside(mousePos, state.board[row][col].box)){
                        // console.log(state.board[row][col])
                        return {id:2, ref:state.board[row][col]}
                    }
                }
            }
        }

        return null;

    }

    let mouseMoveHandler = function(evt, elapsedTime) {
        state.mouseData.pos = mouseInput.getMousePos(canvas, evt);
    }

    let upClickHandler = function(evt, elapsedTime) {
        state.mouseData.down = false;

        // I need to modify placement as sells before this 
        // I need to see what the mouseOn type is and if it is the exact same obj as downIn, and a cell then its a select.
        //      If mouseOn is a cell and downIn was a merch obj then we have an attempted purchase

        // This can return null
        releasedOnObj = userMouseOn(evt);
        // if released over a cell
        if(releasedOnObj !== null && releasedOnObj.ref.hasOwnProperty('selected') && state.mouseData.downIn !== null){

            // if originally clicked object is a store.merch object
            if(state.mouseData.downIn.hasOwnProperty('name')){
                if(state.mouseData.downIn.hasOwnProperty('turret')){
                // if (!state.mouseData.downIn.name === "Upgrade" && !state.mouseData.downIn.name === "Sale" && !state.mouseData.downIn.name === "Unselect"){
                    if(releasedOnObj.ref.turret === null){

                        // console.log("tried to buy")
                        // The buying process is to take the downIn merch reference and take cost from stats if possible
                        if(state.stats.cash - state.mouseData.downIn.turret.price >= 0){
                            // Here I will check to see if the move was on the shortest path
                            // Then check if during a wave or not
                            // Then I will need to check all spider locations and change their paths

                            if(attemptToBlockPath(releasedOnObj.ref, state.mouseData.downIn)){
                                // ############# SUCCESFUL PURCHASE HERE ####################
                                state.stats.cash -= state.mouseData.downIn.turret.price;

                                // at the moment I am passing a reference from the store to the board, I need to pass a copy as to not later modify the store
                                releasedOnObj.ref.turret = Object.assign({}, state.mouseData.downIn.turret);

                                if(state.spiderManager.spiderNearCell(releasedOnObj.ref)){
                                    state.stats.cash += state.mouseData.downIn.turret.price;
                                    releasedOnObj.ref.turret = null;
                                }
                                else{
                                    state.pathFinder.setGraph(state.board);
                                    let noSpiderLeftBehind = state.spiderManager.reInitAllSpiderPaths(state.pathFinder, state.board, mouseInput, state.waves[(state.stats.level + 1) % 2].end.graphID);
                                    // if reinit comes back false I need to cancel the move I just made
                                    if(noSpiderLeftBehind){
                                        state.turretsOnBoard.push(releasedOnObj.ref)
                                    }
                                    else{
                                        // set back board state
                                        state.stats.cash += state.mouseData.downIn.turret.price;
                                        releasedOnObj.ref.turret = null;
                                    }
                                }
                                // This is the cell that will have the turret. I will need a list of cells with turrets to check for collision detection
                            }
                        }
                    }
                }
            }
            
            if(state.mouseData.downIn.hasOwnProperty('selected')){
                if(state.mouseData.downIn.turret !== null){
                    if(releasedOnObj.ref === state.mouseData.downIn){
                        // Selecting Logic
                        if(releasedOnObj.ref.selected){
                            // deselect a cell
                            // I need to find and remove the cell releasedOn
                            state.selectedCells = state.selectedCells.filter(cell => cell !== releasedOnObj.ref);
                            state.selectedCells = state.selectedCells.filter(cell => cell !== state.mouseData.downIn);

                            console.log(state.selectedCells)
                            releasedOnObj.ref.selected = false;
                        } 
                        else{
                            // console.log("tried to select turret")
                            releasedOnObj.ref.selected = true;
                            state.selectedCells.push(releasedOnObj.ref);
                        }
                    }
                    // Moving logic
                    else{
                        if(releasedOnObj.ref.turret === null){
                            // ############# SUCCESFUL MOVE HERE ####################
                            // Here I will check to see if the move was on the shortest path
                            // Then check if during a wave or not
                            // Then I will need to check all spider locations and change their paths


                            if(attemptToBlockPath(releasedOnObj.ref, state.mouseData.downIn)){
                                // if a selected turret is moved it needs to be deselected first
                                state.selectedCells = state.selectedCells.filter(cell => cell !== state.mouseData.downIn);
                                state.selectedCells = state.selectedCells.filter(cell => cell !== releasedOnObj.ref);
                                releasedOnObj.ref.turret = state.mouseData.downIn.turret;
                                state.mouseData.downIn.turret = null;

                                // 
                                if(state.spiderManager.spiderNearCell(releasedOnObj.ref)){
                                    state.mouseData.downIn.turret = releasedOnObj.ref.turret;
                                    releasedOnObj.ref.turret = null;
                                }
                                else{
                                    state.pathFinder.setGraph(state.board);
                                    let noSpiderLeftBehind = state.spiderManager.reInitAllSpiderPaths(state.pathFinder, state.board, mouseInput, state.waves[(state.stats.level + 1) % 2].end.graphID);

                                    if(noSpiderLeftBehind){
                                        // on a move I need to find the cell reference in my list of turretsOnBoard and remove the specific cell
                                        let i = state.turretsOnBoard.findIndex(cell => cell === state.mouseData.downIn);
                                        state.turretsOnBoard.splice(i,1);
                                        state.turretsOnBoard.push(releasedOnObj.ref)
                                    }
                                    else{
                                        state.mouseData.downIn.turret = releasedOnObj.ref.turret;
                                        releasedOnObj.ref.turret = null;
                                    }
                                }


                            }

                        }
                    }
                }
            }
        }
        if(state.selectedCells.length > 0){
            if(releasedOnObj !== null && releasedOnObj.ref.hasOwnProperty('name') && state.mouseData.downIn.hasOwnProperty('name')){
                if(releasedOnObj.ref.name === "Upgrade" && state.mouseData.downIn.name === "Upgrade"){
                    upgradeTurrets();
                }
                else if(releasedOnObj.ref.name === "Sale" && state.mouseData.downIn.name === "Sale"){
                    saleTurrets();

                }
                else if(releasedOnObj.ref.name === "Unselect" && state.mouseData.downIn.name === "Unselect"){
                    for(let i = 0; i < state.selectedCells.length; i++){
                        state.selectedCells[i].selected = false;
                    }
                    state.selectedCells = []
                }
            }
        }
        state.mouseData.downIn = null;
        state.mouseData.turret = null;
    }

    function saleTurrets(){
        for(let i = 0; i < state.selectedCells.length; i++){
            if(state.selectedCells[i].turret.tier > 1){
                state.stats.cash += ((state.selectedCells[i].turret.price*(1+state.selectedCells[i].turret.tier*0.5))/2)
                console.log("Sold for: " + ((state.selectedCells[i].turret.price*(1+(state.selectedCells[i].turret.tier-1)*0.5))/2))

                state.selectedCells[i].turret = null;

                state.pathFinder.setGraph(state.board);
                let path = state.pathFinder.calcShortestPath(state.waves[(state.stats.level + 1) % 2].start.graphID,state.waves[(state.stats.level + 1) % 2].end.graphID,state.board);
                setShortestPathOnBoard(path);

                state.spiderManager.reInitAllSpiderPaths(state.pathFinder, state.board, mouseInput, state.waves[(state.stats.level + 1) % 2].end.graphID);


                // sold turrets need to be removed from the collision detection
                let j = state.turretsOnBoard.findIndex(cell => cell === state.selectedCells[i]);

                if(j > -1){
                    state.turretsOnBoard.splice(j,1);
                }

            }
            else if(state.selectedCells[i].turret.tier == 1){
                state.stats.cash += ((state.selectedCells[i].turret.price)/2)
                console.log("Sold for: " + ((state.selectedCells[i].turret.price*(1+(state.selectedCells[i].turret.tier-1)*0.5))/2))

                state.selectedCells[i].turret = null;

                state.pathFinder.setGraph(state.board);
                let path = state.pathFinder.calcShortestPath(state.waves[(state.stats.level + 1) % 2].start.graphID,state.waves[(state.stats.level + 1) % 2].end.graphID,state.board);
                setShortestPathOnBoard(path);

                state.spiderManager.reInitAllSpiderPaths(state.pathFinder, state.board, mouseInput, state.waves[(state.stats.level + 1) % 2].end.graphID);

                // sold turrets need to be removed from the collision detection
                let j = state.turretsOnBoard.findIndex(cell => cell === state.selectedCells[i]);

                if(j > -1){
                    state.turretsOnBoard.splice(j,1);
                }
            }
        }
        state.selectedCells = [];
    }

    function upgradeTurrets(){
        let sum = 0;
        for(let i = 0; i < state.selectedCells.length; i++){
            // check to see if the turret has tiers left to upgrade
            if (state.selectedCells[i].turret.tier + 1 < 4){
                sum += state.selectedCells[i].turret.price*(1+state.selectedCells[i].turret.tier*0.5);
            }
        }

        if(state.stats.cash > sum){
            // here we upgrade
            // console.log("Upgrading turrets")
            for(let i = 0; i < state.selectedCells.length; i++){
                // check to see if the turret has tiers left to upgrade
                if (state.selectedCells[i].turret.tier + 1 < 4){
                    state.selectedCells[i].turret.tier += 1;
                    if (state.selectedCells[i].turret.tier == 2){
                        state.selectedCells[i].turret.image = state.selectedCells[i].turret.imageUp2

                    }
                    if (state.selectedCells[i].turret.tier == 3){
                        state.selectedCells[i].turret.image = state.selectedCells[i].turret.imageUp3

                    }
                }
            }
            state.stats.cash -= sum;
        }
    }

    let downClickHandler = function(evt, elapsedTime) {
        clickObj = userMouseOn(evt);
        if(clickObj !== null){
            // console.log("click changed state")
            state.mouseData.down = true;
            state.mouseData.downIn = clickObj.ref;
        }
    }


    // This function will be passed both cell and merch objects as originObject. Both have turret and box attributes
    function attemptToBlockPath(cellAttempted, originObject){
        // check to see if the cell attempted to be modified was on the shortest path
        let found = null;
        found = state.enemyPaths.find(cell => cell === cellAttempted);
        
        // the cell the user attempted to place on was not on the paths so approve move
        if(found === undefined){
            return true;
        }

        // moving forward I need to know if there is a wave in progress
        if(!state.isWaveOn){
            // here I will simply check to make sure the new shortest path is completable if the turret was placed.
            
            // set turret then test
            cellAttempted.turret = originObject.turret;
            state.pathFinder.setGraph(state.board);
            // I need to know which spawn points to test with
            // path will be undefined if the turret placement was illegal
            let path = state.pathFinder.calcShortestPath(state.waves[(state.stats.level + 1) % 2].start.graphID,state.waves[(state.stats.level + 1) % 2].end.graphID,state.board);

            // now that the test result is back, set conditions back to before test.
            cellAttempted.turret = null;

            if(path === undefined){
                return false;
            }
            // I need to change the path that is now in the enemy paths lists that corresponds 
            setShortestPathOnBoard(path);

            return true            
            // this needs to be conditional on path, but I dont know what path will return on no path available
        }
        else {

            if(state.spiderManager.spiderNearCell(cellAttempted)){
                return false;
            }
            
            // set turret then test
            cellAttempted.turret = originObject.turret;
            state.pathFinder.setGraph(state.board);
            // I need to know which spawn points to test with
            // path will be undefined if the turret placement was illegal
            let path = state.pathFinder.calcShortestPath(state.waves[(state.stats.level + 1) % 2].start.graphID,state.waves[(state.stats.level + 1) % 2].end.graphID,state.board);

            // now that the test result is back, set conditions back to before test.
            cellAttempted.turret = null;

            if(path === undefined){
                return false;
            }

            // Right here before I approve the placement of the turret is where I will reinitialize all spider paths

            // I need to change the path that is now in the enemy paths lists that corresponds 
            setShortestPathOnBoard(path);
            return true          
        }
    }
    // that.registerHandler = function(handler, type, requireCapture)
    // console.log(mouseInput)

    function buildboard(){
        xOffset = state.dimensions.offset;
        yOffset = state.dimensions.offset;
        // build game board
        // build rows
        for(let r = 0; r < state.dimensions.boardSize; r++){
            let row = []
            // build col
            for(let c = 0; c < state.dimensions.boardSize; c++){
                let cell = {
                    box:{
                        x:xOffset,
                        y:yOffset,
                        width:state.dimensions.cellSize,
                        height:state.dimensions.cellSize,
                    },
                    row:r,
                    col:c,
                    turret:null,
                    selected:false,
                    onShortPath:false,
                    n:null,
                    e:null,
                    w:null,
                    s:null,

                }
                xOffset+=state.dimensions.cellSize;
                row.push(cell)
            }
            xOffset=state.dimensions.offset;
            yOffset+=state.dimensions.cellSize;
            state.board.push(row)
        }
        // build store boxes for mouse operations and rendering. These boxes positions are static, so a one time calculation makes more sense anyway
        let shift = COORD_SIZE*.15-65;

        for(let i = 0; i < state.store.merch.length; i++){
            state.store.merch[i].box = {x:shift + 65, y:COORD_SIZE - COORD_SIZE*.08, width:65, height:65}
            shift += 65
        }

        // build store feature boxes
        for(let i = 0; i < state.store.features.length; i++){
            state.store.features[i].box = {x:shift + 65, y:COORD_SIZE - COORD_SIZE*.08, width:65, height:65}
            shift += 65
        }

        //build spawn points 
        state.spawnPoints = {
            N:{
                box:{
                    x:state.dimensions.offset+5*state.dimensions.cellSize,
                    y:state.dimensions.offset-2*state.dimensions.cellSize,
                    width:state.dimensions.cellSize*2,
                    height:state.dimensions.cellSize*2,
                },
                graphID:state.dimensions.boardSize*state.dimensions.boardSize
            },
            W:{
                box:{
                    x:state.dimensions.offset-2*state.dimensions.cellSize,
                    y:state.dimensions.offset+5*state.dimensions.cellSize,
                    width:state.dimensions.cellSize*2,
                    height:state.dimensions.cellSize*2,
                },
                graphID:state.dimensions.boardSize*state.dimensions.boardSize + 1,
            },
            E:{
                box:{
                    x:state.dimensions.offset+state.dimensions.boardSize*state.dimensions.cellSize,
                    y:state.dimensions.offset+5*state.dimensions.cellSize,
                    width:state.dimensions.cellSize*2,
                    height:state.dimensions.cellSize*2,
                },
                graphID:state.dimensions.boardSize*state.dimensions.boardSize + 2,
            },
            S:{
                box:{
                    x:state.dimensions.offset+5*state.dimensions.cellSize,
                    y:state.dimensions.offset+state.dimensions.boardSize*state.dimensions.cellSize,
                    width:state.dimensions.cellSize*2,
                    height:state.dimensions.cellSize*2,
                },
                graphID:state.dimensions.boardSize*state.dimensions.boardSize + 3,
            },
        }

        state.currentSpawns = {
            start:state.spawnPoints.W,
            end:state.spawnPoints.E
        }

        state.waves = [
            {
                start:state.spawnPoints.W,
                end:state.spawnPoints.E,
            },
            {
                start:state.spawnPoints.N,
                end:state.spawnPoints.S,
            }
        ]

    }

    function setShortestPathOnBoard(path){
        if (state.enemyPaths.length > 0){
            for(let i = 0; i < state.enemyPaths.length;i++){
                state.enemyPaths[i].onShortPath = false;
            }
        }
        // remove the first and last elements 
        let start = path.splice(0,1);
        let end = path.splice(path.length-1,1);

        let newPath = [];
        // convert to col, row values
        for(let i = 0; i < path.length; i++){
            let row = Math.floor(path[i]/state.dimensions.boardSize);
            let col = path[i]%state.dimensions.boardSize

            state.board[row][col].onShortPath = true;
            newPath.push(state.board[row][col])
        }
        state.enemyPaths = newPath;
    }

    function startNextWave(){
        // waves will start at 15 sec and grow 5 seconds each time alternating the directions
        if(state.isWaveOn === false){
            state.isWaveOn = true;
        }

    }

    let upgradeKeyHandler = function(){
        upgradeTurrets();
    }

    let saleKeyHandler = function(){
        saleTurrets();
    }

    let startWaveKeyHandler = function(){
        startNextWave();
    }

    function initRender(){
        // load each turret Up 1
        for (let i = 0; i < state.store.merch.length; i++){
            // how to load background
            state.store.merch[i].turret.imageUp1 = new Image();
            state.store.merch[i].turret.imageUp1.isReady = false;

            state.store.merch[i].turret.imageUp1.onload = function() {
                this.isReady = true;
            };

            state.store.merch[i].turret.imageUp1.src = state.store.merch[i].turret.src.one;
        }

        for (let i = 0; i < state.store.merch.length; i++){
            // for each turret in the store.merch add an attribute image and set imageUp1 to it.
            state.store.merch[i].turret.image = state.store.merch[i].turret.imageUp1;
        }


        // load each turret Up 2
        for (let i = 0; i < state.store.merch.length; i++){
            // how to load background
            state.store.merch[i].turret.imageUp2 = new Image();
            state.store.merch[i].turret.imageUp2.isReady = false;

            state.store.merch[i].turret.imageUp2.onload = function() {
                this.isReady = true;
            };

            state.store.merch[i].turret.imageUp2.src = state.store.merch[i].turret.src.two;
        }    

        // load each turret Up 3
        for (let i = 0; i < state.store.merch.length; i++){
            // how to load background
            state.store.merch[i].turret.imageUp3 = new Image();
            state.store.merch[i].turret.imageUp3.isReady = false;

            state.store.merch[i].turret.imageUp3.onload = function() {
                this.isReady = true;
            };

            state.store.merch[i].turret.imageUp3.src = state.store.merch[i].turret.src.three;
        }

    }

    function initialize(){
        // registerHandler = function(handler, key, repeat, rate)
        initRender();
        state.keyboard.registerHandler(upgradeKeyHandler, TD.controlState.controls[0].key, false, 1);
        state.keyboard.registerHandler(saleKeyHandler, TD.controlState.controls[1].key, false, 1);
        state.keyboard.registerHandler(startWaveKeyHandler, TD.controlState.controls[2].key, false, 1);

        // mouse types are 0: mouseMove, 1: mouseUp, 2:mouseDown
        handlers.push({id:mouseInput.registerHandler(mouseMoveHandler, 0, false), type:1});
        handlers.push({id:mouseInput.registerHandler(upClickHandler, 1, false), type:1});
        handlers.push({id:mouseInput.registerHandler(downClickHandler, 2, false), type:1});
        state.initialized = true;

        buildboard();
        // pathfinding needs to be after the board is built
        state.pathFinder = Components.utilities.shortestPathSolver();
        let path = state.pathFinder.calcShortestPath(state.waves[(state.stats.level + 1) % 2].start.graphID, state.waves[(state.stats.level + 1) % 2].end.graphID, state.board);

        setShortestPathOnBoard(path);
        // pathFinder.getShortestPath(5, 120, state.board);

    }

    function processUserInputs(elapsedTime){
        mouseInput.update(elapsedTime);
        state.keyboard.update(elapsedTime)
        // check for mouse movement when mouse is held down. I considered leaving this in a handler, but
        //      that means I could be processing several mouseMove handlers for each frame. This will be just one mouse move operation per frame. 
        if (state.mouseData.down){
            if(state.mouseData.downIn !== null){
                // I need to be able to reference board cells like other objects with box attributes for this to work or itll need two seperate exceptions
                if(!mouseInput.isInside(state.mouseData.pos, state.mouseData.downIn.box)){
                    // drag if turret
                    // console.log('mouse not in downIn obj')
                    if(state.mouseData.downIn.hasOwnProperty('turret')){
                        if(state.mouseData.downIn.turret !== null){
                            // cells don't have name attributes so that will be the identifying attribute
                            if (state.mouseData.downIn.hasOwnProperty('name')){ // checks to see if dragged from merch
                                // console.log('buying a turret')
                                state.mouseData.turret = state.mouseData.downIn.turret;
                            }
                            else { // checks to see if dragged from board
                                // console.log('moving a turret')
                                state.mouseData.turret = state.mouseData.downIn.turret;

                            }
                        }
                    }
                }
                // else{
                //     console.log('mouse in downIn obj')
                // }
            }
        }
        
    }

    function crossProd(v1,v2){
        return (v1.x*v2.y)-(v1.y*v2.x);
    }

    function testTolerance(val, test, tol){
        if(Math.abs(val-test) < tol){
            return true;
        } else {
            return false;
        }
    }

    // THIS IS THE DEMO CODE PROVIDED BY DOCTOR MATHIAS
    function computeAngle(theta, cellCent, targCent){
        let v1 = {
            x:Math.cos(theta),
            y:Math.sin(theta)
        }
        let v2 = {
            x: targCent.x - cellCent.x,
            y: targCent.y - cellCent.y,
        }
        let dp =null;
        let angle = null;

        v2.len = Math.sqrt(v2.x*v2.x+v2.y*v2.y)
        v2.x/=v2.len
        v2.y/=v2.len

        dp = v1.x*v2.x + v1.y*v2.y;
        angle = Math.acos(dp)

        let cp = crossProd(v1,v2);
        
        return {angle:angle, cp:cp}
    }

    function updateTurrets(elapsedTime){
        for(let i = 0; i < state.turretsOnBoard.length; i++){
            let cellsCenterX = state.turretsOnBoard[i].box.x+state.turretsOnBoard[i].box.width/2;
            let cellsCenterY = state.turretsOnBoard[i].box.y+state.turretsOnBoard[i].box.height/2;
            let test = state.spiderManager.collidesWithSpiders({x:cellsCenterX, y:cellsCenterY,r:state.turretsOnBoard[i].turret.range});
            if(test.collision){
                // This is where I need to update the turret. I need to rotate the turret some unit over time towards the spider it collided with
                // I'll need to design a way to tell which spider was collided with first and target it until it is no longer in range
                
                result = computeAngle(state.turretsOnBoard[i].turret.theta, {x:cellsCenterX,y:cellsCenterY}, {x:test.target.x,y:test.target.y});
                let turnFactor = null;
                if(result.cp > 0){
                    // turn counter clockwise over time
                    turnFactor = 1
                }
                else{
                    // turn clockwise over time 
                    turnFactor = -1
                }

                // turn accordingly to the turn factor
                // allows two degrees of error
                if (testTolerance(result.angle, 0, .07) === false){
                    if(state.turretsOnBoard[i].turret.turnTimer + elapsedTime <= 50){
                        state.turretsOnBoard[i].turret.turnTimer += elapsedTime;
                    }
                    else{
                        state.turretsOnBoard[i].turret.turnTimer += elapsedTime;
                        state.turretsOnBoard[i].turret.turnTimer -= 50;
                        state.turretsOnBoard[i].turret.theta += turnFactor*Math.PI/24
                    }
                }
                else{
                    //elapsedTime, turret, target, centerX, centerY
                    Components.gameObjects.sounds['pelletShooter'].play();
                    state.turretsOnBoard[i].turret.particleAdder(elapsedTime, state.turretsOnBoard[i].turret, test.target, cellsCenterX, cellsCenterY, test.spider);
                }
            }

        }
    }

    function updateParticleSystems(elapsedTime){
        state.particleManager.update(elapsedTime);
    }

    function updateEnemies(elapsedTime){
        state.spiderManager.update(elapsedTime);
    }

    function setHighScores(){
        let tmpScore1 = 0;
        let permaStore = false;
        //screenHighScore:{state:{highScores
        for(let i = 0; i < TD.scoreState.highScores.length; i++){
            console.log(state.stats.score)
            if(permaStore == false){
                if (TD.scoreState.highScores[i] < state.stats.score){
                    permaStore = true;

                    // I found a lower score
                    TD.scoreState.highScores.splice(i,0,state.stats.score)
                    TD.scoreState.highScores.pop();
                }
            }
        }

        if(permaStore == true){
            window.localStorage.setItem('GrubKillerScores', JSON.stringify(TD.scoreState.highScores));
            console.log("From level 2 set new high scores in local storage")
            console.log(JSON.stringify(TD.scoreState.highScores));

        }
    }

    function cleanGame(){
        state.spiderManager.cleanSpiders();
        removerHandlers();
        setHighScores();
        initState();

        Components.gameObjects.sounds['gamePlayMusic'].currentTime=0;
        Components.gameObjects.sounds['gamePlayMusic'].pause()

        Components.gameObjects.sounds['menuMusic'].currentTime=0;
        Components.gameObjects.sounds['menuMusic'].play();

        // This call needs to be moved to wave completions 
        TD.currentScreen = TD.menuScreen;
    }

    function updateWaves(elapsedTime){
        // when isWaveOn is set to true. begin reducing the wavelength. While wavelength is greater than zero randomly spawn spiders

        if(state.isWaveOn){
            if(state.waveTimer > 0){
                state.waveTimer -= elapsedTime;

                // Here we need to spawn a spider over a random time interval
                state.currentSpawns.start = state.waves[(state.stats.level + 1) % 2].start;
                state.currentSpawns.end = state.waves[(state.stats.level + 1) % 2].end;

                
                if(state.spiderInterval === null){
                    state.spiderInterval = Math.floor(Math.random() * (500 - 100) + 100);
                    state.spiderManager.addSpiders(1,state.waves[(state.stats.level + 1) % 2].start.box.x+state.waves[(state.stats.level + 1) % 2].start.box.width/2,state.waves[(state.stats.level + 1) % 2].start.box.y+state.waves[(state.stats.level + 1) % 2].start.box.height/2, state.waves[(state.stats.level + 1) % 2].end.box.x+state.waves[(state.stats.level + 1) % 2].end.box.width/2,state.waves[(state.stats.level + 1) % 2].end.box.y+state.waves[(state.stats.level + 1) % 2].end.box.height/2, state.enemyPaths);
                }

                if(state.spiderIntervalTimer + elapsedTime <= state.spiderInterval){
                    state.spiderIntervalTimer += elapsedTime;
                }
                else{
                    // time breached, set new random time
                    state.spiderIntervalTimer += elapsedTime;
                    state.spiderIntervalTimer -= state.spiderInterval;

                    // set new random time interval for dispersing spiders
                    state.spiderInterval = Math.floor(Math.random() * (500 - 100) + 100);

                    state.spiderManager.addSpiders(1,state.waves[(state.stats.level + 1) % 2].start.box.x+state.waves[(state.stats.level + 1) % 2].start.box.width/2,state.waves[(state.stats.level + 1) % 2].start.box.y+state.waves[(state.stats.level + 1) % 2].start.box.height/2, state.waves[(state.stats.level + 1) % 2].end.box.x+state.waves[(state.stats.level + 1) % 2].end.box.width/2,state.waves[(state.stats.level + 1) % 2].end.box.y+state.waves[(state.stats.level + 1) % 2].end.box.height/2, state.enemyPaths);
                }
            }
            else {
                state.isWaveOn = false;
                state.stats.level+=1;
                state.spiderInterval = null;
                state.waveTimer += (10000 + state.stats.level*5000);

                let path = state.pathFinder.calcShortestPath(state.waves[(state.stats.level + 1) % 2].start.graphID,state.waves[(state.stats.level + 1) % 2].end.graphID,state.board);
                setShortestPathOnBoard(path);

            }
        }

    }

    function update(elapsedTime){
        if(state.endGame == false){
            if(state.initialized === false){
                initialize();
            }
            updateEnemies(elapsedTime);
            updateTurrets(elapsedTime);
            updateParticleSystems(elapsedTime);
            processUserInputs(elapsedTime);
            updateWaves(elapsedTime);
    
        }
        else{
            if(state.endGameTimer + elapsedTime <= 1000){
                state.endGameTimer += elapsedTime;
            }
            else{
                state.endGameTimer += elapsedTime;
                state.endGameTimer -= 1000;
                state.endGameCount -=1
                console.log(state.endGameCount)
                if(state.endGameCount == 0){
                    cleanGame();
                }
            }
        }
    }
    // Now all update methods
    return {update}
}(Components.mouseInput, TD.gameState));
