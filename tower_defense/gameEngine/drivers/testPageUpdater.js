Drivers.testPageUpdater = (function(state){
    // This must be the first thing in hte update method to preserve the processInput -> update -> render, model
    // processInput();

    // let state = LunarLanding.state;

    function update(elapsedTime){
        if(state.recordedTime + elapsedTime <= 1000){
            state.recordedTime += elapsedTime;
        }
        else{
            state.recordedTime += elapsedTime;
            state.recordedTime = state.recordedTime - 1000;
            state.count += 1;
        }
    }
    // Now all update methods
    return {update}
}(TD.testState));
