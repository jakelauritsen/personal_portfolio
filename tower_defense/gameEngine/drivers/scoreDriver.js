Drivers.scores = (function(mouseInput, state){

    let handlers = []
    // let mouseInput = Components.input.mouse();
    state.initialized = false;
    state.highScores = [0,0,0,0,0]


    if(!window.localStorage.getItem('GrubKillerScores')){
        populateStorage();
    }
    else{
        setScores();
    }

    function populateStorage(){
        window.localStorage.setItem('GrubKillerScores', JSON.stringify(state.highScores));
        console.log("Set up a local storage with " + JSON.stringify(state.highScores))
    }

    function setScores(){
        state.highScores = JSON.parse(window.localStorage.getItem('GrubKillerScores'));
        console.log("Found local storage: ")
        console.log(JSON.parse(window.localStorage.getItem('GrubKillerScores')))
    }

    function removerHandlers() {
        if (handlers.length > 0) {
            for (let i = 0; i < handlers.length; i++){
                mouseInput.unregisterHandler(handlers[i].type,handlers[i].id);
            }
        }
    }

    let clickHandler = function(evt, elapsedTime) {
        canvas = document.getElementById('canvas-main');
        context = canvas.getContext('2d');
        // console.log("made it in handler")
        var mousePos = mouseInput.getMousePos(canvas, evt);
    
        if(mouseInput.isInside(mousePos, state.menuButton)){
            removerHandlers();
            state.initialized = false;
            TD.currentScreen = TD.menuScreen;
        }
    }

    // that.registerHandler = function(handler, type, requireCapture)
    // console.log(mouseInput)
    // This needs to be reinitialized every time the page is navigated to.

    function initialize(){
        handlers.push({id:mouseInput.registerHandler(clickHandler, 1, false), type:1});
        state.initialized = true;
    }

    function update(elapsedTime){
        if (state.initialized === false){
            initialize();
        }
        
        mouseInput.update(elapsedTime);
    }
    // Now all update methods
    return {update}
}(Components.mouseInput, TD.scoreState));