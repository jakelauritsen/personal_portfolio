Drivers.menu = (function(mouseInput, state){
    let handlers = []
    // let mouseInput = Components.input.mouse();
    state.initialized = false;

    function navigate(screenName){
        if (screenName == "New Game"){
            state.initialized = false;
            removerHandlers();
            Components.gameObjects.sounds['menuMusic'].currentTime=0;
            Components.gameObjects.sounds['menuMusic'].pause()
    
            Components.gameObjects.sounds['gamePlayMusic'].currentTime=0;
            Components.gameObjects.sounds['gamePlayMusic'].play();
            TD.currentScreen = TD.gameScreen;
        }
        else if (screenName == "High Scores"){
            state.initialized = false;
            removerHandlers();
            TD.currentScreen = TD.scoreScreen;
        }
        else if (screenName == "Controls"){
            state.initialized = false;
            removerHandlers();
            TD.currentScreen = TD.controlScreen;
        }
        else if (screenName == "Credits"){
            state.initialized = false;
            removerHandlers();
            TD.currentScreen = TD.creditsScreen;
        }

    }


    let clickHandler = function(evt, elapsedTime) {
        canvas = document.getElementById('canvas-main');
        context = canvas.getContext('2d');
        // console.log("made it in handler")
        var mousePos = mouseInput.getMousePos(canvas, evt);
    
        for(let i = 0; i < state.buttons.length; i++){
            if(mouseInput.isInside(mousePos, state.buttons[i])){
                navigate(state.buttons[i].name)
            }
        }  
    }

    function removerHandlers() {
        if (handlers.length > 0) {
            for (let i = 0; i < handlers.length; i++){
                mouseInput.unregisterHandler(handlers[i].type,handlers[i].id);
            }
        }
    }

    let soundHandler = function(){
        Components.gameObjects.sounds['menuMusic'].play()
        window.removeEventListener('click', soundHandler)
    }

    // that.registerHandler = function(handler, type, requireCapture)
    // console.log(mouseInput)
    // This will create a handler then return an id for storing until I need to unregister the handler
    function initialize(){
        handlers.push({id:mouseInput.registerHandler(clickHandler, 1, false), type:1});
        window.addEventListener('click', soundHandler)
        state.initialized = true;
    }

    function update(elapsedTime){
        if (state.initialized === false){
            initialize();
        }
        mouseInput.update(elapsedTime);
    }
    // Now all update methods
    return {update}
}(Components.mouseInput, TD.menuState));
