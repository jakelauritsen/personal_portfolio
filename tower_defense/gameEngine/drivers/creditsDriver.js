Drivers.credits = (function(mouseInput, state){

    let handlers = []
    // let mouseInput = Components.input.mouse();
    state.initialized = false;

    function removerHandlers() {
        if (handlers.length > 0) {
            for (let i = 0; i < handlers.length; i++){
                mouseInput.unregisterHandler(handlers[i].type,handlers[i].id);
            }
        }
    }

    let clickHandler = function(evt, elapsedTime) {
        canvas = document.getElementById('canvas-main');
        context = canvas.getContext('2d');
        // console.log("made it in handler")
        var mousePos = mouseInput.getMousePos(canvas, evt);
    
        if(mouseInput.isInside(mousePos, state.menuButton)){
            removerHandlers();
            TD.currentScreen = TD.menuScreen;
            state.initialized = false;
        }
    }

    // that.registerHandler = function(handler, type, requireCapture)
    // console.log(mouseInput)

    function initialize(){
        handlers.push({id:mouseInput.registerHandler(clickHandler, 1, false), type:1});
        state.initialized = true;
    }

    function update(elapsedTime){
        if(state.initialized === false){
            initialize();
        }
        mouseInput.update(elapsedTime);
    }
    // Now all update methods
    return {update}
}(Components.mouseInput, TD.creditState));
