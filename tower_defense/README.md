# Overview
This game was the second and the most recent game that I developed. It uses a node server to serve the necessary files to the browser in a logical order. I was strict about enforcing an object oriented model, and I think it paid off. It made the game easier to update. The game is "incomplete," but all functionality is in place. The only element not implemented is particle and weapon effects for the special turrets. 

## Technical Specifications
- AI operated intelligent spiders
- Waves of randomly spawned spiders
- Animated spiders
- Particle effects
- Intelligent driven turrets

## Running Program 
The game must be run as a node server: `node server.js`, then with the server running, connect to: `http://localhost:3000`
 
