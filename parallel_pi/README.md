# Overview
The purpose of including this program is to demonstrate my understanding of the JVM threading tools and parallel programming methods. I have implemented a thread pool that builds itself to the size of the machine it runs on. It then builds 1000 "tasks," where each task is a container of logic that knows how to calculate its own digit of pi by its index using a Taylor Series. I suggest running some process manager that allows you to watch core utilization. I used htop on my linux OS. You'll see 100% work efficiency. The algorithm is not the most effecient, but that is by design to have enough work load to parallelize. 

## Technical Specifications
- A progress indicator (symbol for every 10 digits calculatated)
- Expect 30 to 50 sec on 4 cores
- Check the calculation with a String of pi 

## Running Program 
I have the program runnable in gradle, but in build/libs there is a jar file that can be ran as well.


