package assn_4;

// This will define each task.
// Each task will need to be given a unique index
// Each task needs a function that bakes the pi fragments

import java.util.LinkedList;

public class Task{
    private int index = 0;
    private String pi_val;

    Task(int i){
        this.index = i;
    }

    public int getIndex() {
        return index;
    }

    public String getPiVal(){
        return this.pi_val;
    }

    public String calculate(){
        if(this.index==0){
            return "3.";
        }
        else {
            int whole_pi = getDecimal(this.index);
            String whole_pi_recipe = Integer.toString(whole_pi);

//      Handles the case if 0 is the leading int
            if (whole_pi_recipe.length() != 9) {
                return "0";
            } else {
                return Character.toString(whole_pi_recipe.charAt(0));
            }
        }
    }

    public int getDecimal(long n) {
        long av, a, vmax, N, num, den, k, kq, kq2, t, v, s, i;
        double sum;

        N = (long) ((n + 20) * Math.log(10) / Math.log(2));

        sum = 0;

        for (a = 3; a <= (2 * N); a = nextPrime(a)) {

            vmax = (long) (Math.log(2 * N) / Math.log(a));
            av = 1;
            for (i = 0; i < vmax; i++)
                av = av * a;

            s = 0;
            num = 1;
            den = 1;
            v = 0;
            kq = 1;
            kq2 = 1;

            for (k = 1; k <= N; k++) {

                t = k;
                if (kq >= a) {
                    do {
                        t = t / a;
                        v--;
                    } while ((t % a) == 0);
                    kq = 0;
                }
                kq++;
                num = mulMod(num, t, av);

                t = (2 * k - 1);
                if (kq2 >= a) {
                    if (kq2 == a) {
                        do {
                            t = t / a;
                            v++;
                        } while ((t % a) == 0);
                    }
                    kq2 -= a;
                }
                den = mulMod(den, t, av);
                kq2 += 2;

                if (v > 0) {
                    t = modInverse(den, av);
                    t = mulMod(t, num, av);
                    t = mulMod(t, k, av);
                    for (i = v; i < vmax; i++)
                        t = mulMod(t, a, av);
                    s += t;
                    if (s >= av)
                        s -= av;
                }

            }

            t = powMod(10, n - 1, av);
            s = mulMod(s, t, av);
            sum = (sum + (double) s / (double) av) % 1;
        }
        return (int) (sum * 1e9); // 1e9 is 9 decimal places
    }

    private long mulMod(long a, long b, long m) {
        return (long) (a * b) % m;
    }

    private long modInverse(long a, long n) {
        long i = n, v = 0, d = 1;
        while (a > 0) {
            long t = i / a, x = a;
            a = i % x;
            i = x;
            x = d;
            d = v - t * x;
            v = x;
        }
        v %= n;
        if (v < 0)
            v = (v + n) % n;
        return v;
    }

    private long powMod(long a, long b, long m) {
        long tempo;
        if (b == 0)
            tempo = 1;
        else if (b == 1)
            tempo = a;

        else {
            long temp = powMod(a, b / 2, m);
            if (b % 2 == 0)
                tempo = (temp * temp) % m;
            else
                tempo = ((temp * temp) % m) * a % m;
        }
        return tempo;
    }

    private boolean isPrime(long n) {
        if (n == 2 || n == 3)
            return true;
        if (n % 2 == 0 || n % 3 == 0 || n < 2)
            return false;

        long sqrt = (long) Math.sqrt(n) + 1;

        for (long i = 6; i <= sqrt; i += 6) {
            if (n % (i - 1) == 0)
                return false;
            else if (n % (i + 1) == 0)
                return false;
        }
        return true;
    }

    private long nextPrime(long n) {
        if (n < 2)
            return 2;
        if (n == 9223372036854775783L) {
            System.err.println("Next prime number exceeds Long.MAX_VALUE: " + Long.MAX_VALUE);
            return -1;
        }
        for (long i = n + 1;; i++)
            if (isPrime(i))
                return i;
    }

}
