package assn_4;

public class WorkRoster implements Runnable{
    private TaskQueue src_queue;
    private ResultTable piMap;

    WorkRoster(TaskQueue src_queue, ResultTable piMap){
        this.src_queue = src_queue;
        this.piMap = piMap;
    }


    @Override
    public void run() {
        while(!this.src_queue.isEmpty()){
            Task tmp_task = this.src_queue.get_next_task();
            String sol = tmp_task.calculate();
            piMap.insertPiSlice(tmp_task.getIndex(), sol);
        }
    }
}
