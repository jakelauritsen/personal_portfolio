package assn_4;

import java.util.HashMap;

public class ResultTable {
    private HashMap<Integer,String> piMap = new HashMap<Integer,String>(1000);

    public synchronized void insertPiSlice(int key, String val){
        this.piMap.put(key,val);
    }

    public String getPi(){
        String piString = "";
        for(int i = 0; i < 1000; i++){
            if(piMap.containsKey(i)){
                piString += piMap.get(i);
            }
            else{
                piString += "*** NO VAL FOR THIS INDEX OF PI WAS COMPUTED OR AT LEAST NOT YET ***";
            }
        }
        return piString;
    }
}
