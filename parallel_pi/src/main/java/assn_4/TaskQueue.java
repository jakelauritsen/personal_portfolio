package assn_4;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

public class TaskQueue {
    private LinkedList<Task> task_queue = new LinkedList<Task>();
    private int tasks_complete = 0;

    TaskQueue(ArrayList<Task> unshuffled_task_list){
        Collections.shuffle(unshuffled_task_list);
        this.task_queue.addAll(unshuffled_task_list);
    }

    public Task get_task_by_index(int index){
        return this.task_queue.get(index);
    }

    public synchronized Task get_next_task(){
        this.tasks_complete++;
        if(tasks_complete % 100 == 0){
            System.out.println(".");
        }
        else if(tasks_complete % 10 == 0){
            System.out.print(".");
        }
        return task_queue.remove();
    }

    public LinkedList<Task> getTaskList() {
        return task_queue;
    }

    public synchronized Boolean isEmpty(){
        return this.task_queue.isEmpty();
    }
}


//    public void print_task_indexes(){
//        for(Task tasks: this.task_queue){
//            System.out.println(tasks.getIndex());
//        }
//    }