# Overview
This is a personal project of mine that I used to view the environmental conditions of my pet snake's enclosure from anywhere in my house. I use a SSH protocol to communicate to a Raspberry Pi on my LAN network, and from the Rasberry Pi I then run a small script for printing to the console the data retrieved by two linked sensors. I didn't provide my source code because it doesn't run without the system anyway, and the images provided show the architecture and completion of the project better (I'm also moving and I haven't found the box with the Pi).

## Technical Specifications
Two DHT22 sensors, one bread board, and a Raspberry Pi. 

