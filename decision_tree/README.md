# Overview
This project for the purpose of demonstrating my skills, is I'll admit, a bit bloated. For the sake of time I've decided to leave the project as is for now. I built this decision tree with the basic source code provided by the instructor. The functions that I implemented can be found in the "bin_id3.py" file. I wrote everything after and including the proportions function. The unit tests found in "bin_id3_uts.py" test varias steps of the algorithm. The entire purpose and focus of this project is in unit test 25. It shows my program succesfully build the decision tree from the few 100's of examples in the csv and return the accuracy of such a tree. This is a unnatural data set, but the algorithm will still handle error if it existed. 

## Technical Specifications
- Using the ID3 algorithm build a decision tree from the list of examples

## Running Program 
- You'll need a python3 interpreter to run the "bin_id3_uts.py" unit tests.

