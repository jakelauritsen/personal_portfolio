import csv
import math
import copy

##################################################
# module: bin_id3.py
# description: Binary ID3 decision tree learning
# Jake Lauritsen
# A02046091
# bugs to vladimir kulyukin on canvas
###################################################

### Positive and Negative Constant labels; don't change
### these.
PLUS  = 'Yes'
MINUS = 'No'

class id3_node(object):

    def __init__(self, lbl):
        self.__label = lbl
        self.__children = {}

    def set_label(self, lbl):
        self.__label = lbl
        
    def add_child(self, attrib_val, node):
        self.__children[attrib_val] = node

    def get_label(self):
        return self.__label

    def get_children(self):
        return self.__children

    def get_child(self, attrib_val):
        assert attrib_val in self.__children
        return self.__children[attrib_val]

class bin_id3(object):

    @staticmethod
    def get_attrib_values(a, kvt):
        """
        Looks up values of attribute a in key-value table.
        """
        return kvt[a]

    @staticmethod
    def get_example_attrib_val(example, attrib):
        """
        Get the value of attribute attrib in example.
        """
        assert attrib in example
        return example[attrib]

    @staticmethod
    def parse_csv_file_into_examples(csv_fp):
        """
        Takes a csv file specified by the path csv_fp and
        converts it into an array of examples, each of which
        is a dictionary of key-value pairs where keys are
        column names and the values are column attributes.
        """
        examples = []
        with open(csv_fp) as csv_file:    
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            key_names  = None
            for row in csv_reader:
                if len(row) == 0:
                    continue
                if line_count == 0:
                    key_names = row
                    for i in range(len(key_names)):
                        ## strip whitespace on both ends.
                        row[i] = row[i].strip()
                        line_count += 1
                else:
                    ex = {}
                    for i, k in enumerate(key_names):
                        ## strip white spaces on both ends.
                        ex[k] = row[i].strip()
                    examples.append(ex)
            return examples, key_names

    @staticmethod
    def construct_attrib_values_from_examples(examples, attributes):
        """
        Constructs a dictionary from a list of examples where each attribute
        is mapped to a list of all its possible values in examples.
        """
        avt = {}
        for a in attributes:
            if not a in avt:
                avt[a] = set()
            for ex in examples:
                if a in ex:
                    if not ex[a] in avt[a]:
                        avt[a].add(ex[a])
                else:
                    avt[a].add(None)
        return avt

    @staticmethod
    def find_examples_given_attrib_val(examples, attrib, val):
        """
        Finds all examples in such that attrib = val.
        """
        rslt = []
        #print('Looking for examples where {}={}'.format(attrib, val))
        for ex in examples:
            if attrib in ex:
                if ex[attrib] == val:
                    rslt.append(ex)
        return rslt

    @staticmethod
    def find_most_common_attrib_val(examples, attrib, avt):
        """
        Finds the most common value of attribute attrib in examples.
        """
        attrib_vals = bin_id3.get_attrib_values(attrib, avt)
        val_counts = {}
        for av in attrib_vals:
            SV = bin_id3.find_examples_given_attrib_val(examples, attrib, av)
            val_counts[av] = len(SV)
        max_cnt = 0
        max_val = None
        #print('val_counts = {}'.format(val_counts))
        for val, cnt in val_counts.items():
            if cnt > max_cnt:
                max_cnt = cnt
                max_val = val
        assert max_val != None
        return max_val, max_cnt

    @staticmethod
    def get_non_target_attributes(target_attrib, attribs):
        """
        Returns a comma separated string of all attributes in the list attribs that
        that are not equal to target_attrib; 
        - target_attrib is a string.
        - attribs is a list of strings.
        """
        return ', '.join([a for a in attribs if a != target_attrib])

    @staticmethod
    def display_info_gains(gains):
        """
        Displays a dictionary of information gains in the format attribute: gain.
        """
        print('Information gains are as follows:')
        for attrib, gain in gains.items():
            print('\t{}: {}'.format(attrib, gain))

    @staticmethod
    def display_id3_node(node, tabs):
        """
        Displays the subtree rooted at a node.
        """
        print(tabs + '{}'.format(node.get_label()))
        children = node.get_children()
        for v, n in children.items():
            print(tabs + '\t{}'.format(v))
            bin_id3.display_id3_node(n, tabs+'\t\t')

    @staticmethod
    def proportion(examples, attrib, val):
        """
        Computes the proportion of examples whose attribute attrib has the value val.
        """
        examples_with_val = bin_id3.find_examples_given_attrib_val(examples,attrib,val)
        assert len(examples) != 0
        return len(examples_with_val)/len(examples)

    @staticmethod
    def entropy(examples, attrib, avt):
        """
        Computes entropy of examples with respect of attribute attrib.
        avt is the attribute value table computed by construct_attrib_values_from_examples().
        """
        sol = 0
        list_of_values_to_target = avt[attrib]
        # print(attrib)
        # print(list_of_values_to_target)
        # I think that examples is already relative to avt, in other words, I think that avt was constructed with the
        #   provided examples

        for val in list_of_values_to_target:
            p_i = bin_id3.proportion(examples,attrib,val)

            if p_i != 0:
                sol += p_i*math.log2(p_i)

        return sol * -1
        
   
    @staticmethod
    def gain(examples, target_attrib, attrib, avt):
        """
        Computes gain of the attribute attrib in examples.
        """
        # uts08 function call -> bin_id3.gain(examples, 'PlayTennis', 'Wind', avt)
        entropy_of_target = bin_id3.entropy(examples,target_attrib,avt)

        summation = 0

        vals_to_attribute = avt[attrib]

        # print(vals_to_attribute)

        for val in vals_to_attribute:
            prop_of_s = len(bin_id3.find_examples_given_attrib_val(examples,attrib,val))/len(examples)
            sub_examples_with_att_val_pairing = bin_id3.find_examples_given_attrib_val(examples,attrib,val)
            if len(sub_examples_with_att_val_pairing) > 0:
                summation += prop_of_s*bin_id3.entropy(sub_examples_with_att_val_pairing, target_attrib, avt)

        return entropy_of_target - summation


    @staticmethod
    def find_best_attribute(examples, target_attrib, attribs, avt):
        """
        Finds the attribute in attribs with the highest information gain.
        This method returns three values: best attribute, its gain, and
        a dictionary that maps each attribute to its gain.
        """
        all_attrib_and_gain = {}
        best_gain = 0
        best_attrib = None
        # Right now I am returning the target attribute 'Play Tennis' as the best attribute to split on, but I think

        for attrib in attribs:
            if attrib != target_attrib:
                gain_for_attrib = bin_id3.gain(examples, target_attrib, attrib, avt)
                if gain_for_attrib > best_gain:
                    best_gain = gain_for_attrib
                    best_attrib = attrib
                all_attrib_and_gain[attrib] = gain_for_attrib

        return best_attrib, best_gain, all_attrib_and_gain


    @staticmethod
    def fit(examples, target_attrib, attribs, avt, dbg):
        """
        Returns a decision tree from examples given target_attribute target_attrib,
        attributes attribs, and attribute-value table.
        - examples is a list of examples;
        - target_attrib is a string (e.g., 'PlayTennis')
        - attribs is a list of attributes (strings)
        - avt is a dictionary constructed by construct_attrib_values_from_examples()
        - dbg is a debug flag True/False. When it is true, then things should
          be printed out as the algorithm computes the decision tree. For example,
          in my implementation I have things like
          if len(SV) == len(examples):
            ## if all examples are positive, then return the root node whose label is PLUS.
            if dbg == True:
                print('All examples positive...')
                print('Setting label of root to {}'.format(PLUS))
            root.set_label(PLUS)
            return root
        """
        global PLUS
        global MINUS

        # Step.1 Create a root node. This needs to be the best attribute to start, but I'm confused how the recursion
        #   would work? I think I'm thinking about the 'root' wrong. It needs to be set to the best attribute
        root = id3_node('tmp')

        # Step.2 Check for all negative target_attib values for each rec call on reducing examples

        all_same_vals = True
        first_found_tattrib_val = None
        assert len(examples) > 0
        for example in examples:
            if first_found_tattrib_val == None:
                first_found_tattrib_val = example[target_attrib]
            elif example[target_attrib] != first_found_tattrib_val:
                all_same_vals = False

        if all_same_vals:
            return id3_node(first_found_tattrib_val)

        # Step.3

        if len(attribs) == 0:
            most_common_val, _ = bin_id3.find_most_common_attrib_val(examples, target_attrib, avt)
            return root.set_label(most_common_val)

        # Step.4 If we've made it this far find best attrib
        assert len(examples) != 0
        ba, bag, all_attr_gains = bin_id3.find_best_attribute(examples,target_attrib, attribs, avt)
        root.set_label(ba)

        # I think this is the values for the ba and its relevant values
        child_node = None
        for bav in avt[ba]:
            split_examples = bin_id3.find_examples_given_attrib_val(examples, ba, bav)
            if len(split_examples) == 0:
                lbl, _ = bin_id3.find_most_common_attrib_val(examples, target_attrib, avt)
                child_node = id3_node(lbl)
            else:
                attribs_copy = copy.copy(attribs)
                attribs_copy.remove(ba)
                child_node = bin_id3.fit(split_examples, target_attrib, attribs_copy, avt, True)
            root.add_child(bav, child_node)

        return root

                   
    @staticmethod
    def predict(root, example):
        """
        Classifies an example given a decision tree whose root is root.
        """
        global PLUS
        global MINUS

        r_lbl = root.get_label()

        if r_lbl == PLUS:
            return PLUS

        if r_lbl == MINUS:
            return MINUS

        r_vals = bin_id3.get_example_attrib_val(example, r_lbl)

        return bin_id3.predict(root.get_child(r_vals), example)

