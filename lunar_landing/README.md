# Overview
This is a client side and javascript-only game built to play like the classic Lunar Landing. The game was built in under two weeks, and was my first complete and playable game. With this first game, I ran into several design issues that I was not satisfied with. In the subsequent tower defense game I developed, I implemented improvements to my original design. Lunar Landing runs well, but I wanted it to be more modulized and object oriented, which I achieved in the tower defense game. The contrast between this Lunar Landing game and the tower defense game demonstrate my ability to learn and improve, complete a project on a tight schedule, and display technical competency. 


## Technical Specifications
- Procedurally generated terrain
- A basic physics engine (momentum and gravity)
- Particle effects
- SFX
- Escalading difficulty
- An overall enjoyable/challenging design

## Running Program
Simply open the index.html file in Chrome or Firefox to play the game, enjoy!
