LunarLanding.render = (function(){
    let canvas = document.getElementById('canvas-main');
    let context = canvas.getContext('2d');
    // The render needs to recieve the game model to me this means the state of the game after a update wrapped into a js object
    function masterRender(){
        context.clearRect(0, 0, canvas.width, canvas.height);

        if(LunarLanding.liveScreen == 'menu'){
            LunarLanding.renderMenu.render(context, canvas);
        }

        else if(LunarLanding.liveScreen == 'credits'){
            LunarLanding.renderCredits.render(context, canvas);
        }
        else if(LunarLanding.liveScreen == 'High Scores'){
            LunarLanding.renderHighScore.render(context, canvas);
        }
        else if(LunarLanding.liveScreen == 'Controls'){
            LunarLanding.renderControls.render(context, canvas);
        }
        else if(LunarLanding.liveScreen == 'configControls'){
            LunarLanding.renderConfigControls.render(context, canvas);
        }
        else if(LunarLanding.liveScreen == 'Level1'){
            LunarLanding.renderLevel1.render(context, canvas);
        }
        else if(LunarLanding.liveScreen == 'Level2'){
            LunarLanding.renderLevel2.render(context, canvas);
        }

    }
    
    return {masterRender}

}());