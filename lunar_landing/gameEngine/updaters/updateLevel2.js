LunarLanding.updateLevel2 = (function(state){
    canvas = document.getElementById('canvas-main');
    context = canvas.getContext('2d');
    COORD_SIZE = 1000;
    // This is for adjustments in angle
    THETA_SLICE = Math.PI/128 // This can be used as a rotate rate
    MOVE_RATE = 150; // I will combined this with passed time in seconds to control pace over time
    
    state.acceleration = 0;
    state.particles = [];
    state.progress="incomplete";
    state.isInitialized = false;
    state.controls = {};
    state.userInputs = [];
    state.terrain = [];
    state.gravityTime = 0;
    state.explosionTime = 0;
    state.explosionDuration = 0;
    state.landerData = {};
    state.timerInitialized = false;
    state.exploding = false;
    state.renderShip = true;
    state.fuel=1000;
    state.speed = 0;



    state.pads = {
        pad1:{
            p1:{x:0,y:0},
            p2:{x:0,y:0},
            type:"pad"
        },
        // pad2:{
        //     p1:{x:0,y:0},
        //     p2:{x:0,y:0},
        //     type:"pad"
        // }
    };

    // src: code from lecture
    let input = (function() {
        function Keyboard() {
            let that = {
                keys : {}
            };
            function keyPress(e) {
                that.keys[e.key] = e.timeStamp;
            }
            function keyRelease(e) {
                delete that.keys[e.key];
            }
            window.addEventListener('keydown', keyPress);
            window.addEventListener('keyup', keyRelease);

            return that;
        }
        return {
            Keyboard : Keyboard
        };
    }());

    let myInput = input.Keyboard();


    // src: https://stackoverflow.com/questions/25582882/javascript-math-random-normal-distribution-gaussian-bell-curve
    function nextGaussian(min, max, skew) {
        let u = 0, v = 0;
        while(u === 0) u = Math.random() //Converting [0,1) to (0,1)
        while(v === 0) v = Math.random()
        let num = Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v )

        num = num / 10.0 + 0.5 // Translate to 0 -> 1
        if (num > 1 || num < 0) 
            num = randn_bm(min, max, skew) // resample between 0 and 1 if out of range

        else{
            num = Math.pow(num, skew) // Skew
            num *= max - min // Stretch to fill range
            num += min // offset to min
        }
        return num
    }

    function setLander(){
        state.landerData = {
            dimensions:{w:60,h:60},
            center:{x:90,y:90},
            theta:Math.PI/2,
            radius:27,
        }
    }

    

    function setPads(){
        // the y coord on all points for both lines needs to be between half of the COORD_SIZE and max COORD_SIZE
        y1 = Math.floor(Math.random()* ((COORD_SIZE * .8) - (COORD_SIZE*.4)) + (COORD_SIZE*.4)) 
        state.pads.pad1.p1.y = y1;
        state.pads.pad1.p2.y = y1;
        // y2 = Math.floor(Math.random()* ((COORD_SIZE * .8) - (COORD_SIZE*.4)) + (COORD_SIZE*.4))
        // state.pads.pad2.p1.y = y2;
        // state.pads.pad2.p2.y = y2;
        
        // Math.floor(Math.random() * (max - min) + min);
        x1 = Math.floor(Math.random() * ((COORD_SIZE - 150) - 50) + 50) 
        // x2 = Math.floor(Math.random() * ((COORD_SIZE - 150) - (COORD_SIZE/2+50)) + (COORD_SIZE/2+50))
        state.pads.pad1.p1.x = x1;
        state.pads.pad1.p2.x = x1 + 120;
        
        // state.pads.pad2.p1.x = x2;
        // state.pads.pad2.p2.x = x2 + 120;
        // console.log("pads updated")
    }

    function setTerrain(){
        let startY = Math.floor(Math.random() * ((COORD_SIZE) - (COORD_SIZE/2)) + (COORD_SIZE/2));
        let endY = Math.floor(Math.random() * ((COORD_SIZE) - (COORD_SIZE/2)) + (COORD_SIZE/2));

        state.terrain.push({ 
            p1:{x:0,y:startY}, 
            p2:{x:state.pads.pad1.p1.x, y:state.pads.pad1.p1.y},
            type:"terrain"
        });

        state.terrain.push(state.pads.pad1);
        
        // state.terrain.push({
        //     p1:{x:state.pads.pad1.p2.x, y:state.pads.pad1.p2.y},
        //     p2:{x:state.pads.pad2.p1.x, y:state.pads.pad2.p1.y},
        //     type:"terrain"
        // });

        // state.terrain.push(state.pads.pad2);
        
        state.terrain.push({
            p1:{x:state.pads.pad1.p2.x, y:state.pads.pad1.p2.y},
            p2:{x:COORD_SIZE, y:endY},
            type:"terrain"
        });
        // I'm going to try and box in the lines so that I can fill in render and have a solid terrain for filling

        state.terrain.push({
            p1:{x:COORD_SIZE, y:endY},
            p2:{x:COORD_SIZE, y:COORD_SIZE},
            type:"edge"
        });
        
        state.terrain.push({
            p1:{x:COORD_SIZE, y:COORD_SIZE},
            p2:{x:0, y:COORD_SIZE},
            type:"edge"
        });
                
        state.terrain.push({
            p1:{x:0, y:COORD_SIZE},
            p2:{x:0, y:startY},
            type:"edge"
        });
        
    }

    function setControls(){
        state.controls = LunarLanding.screenControls.state.controls;
    }

    function roughTerrain(){
        // I need to be able to identify the line as a pad or I'll break the landing zones.
        // For that reason I added a type to each line.
        let s = 1.2;
        for(let j = 0; j < 6; j++){
        
        // This will run indefinetly. The terrain list grows each time it runs
        // just kidding I fixed it.
            let tmpTurrain = []
            for(let i = 0; i < state.terrain.length; i++){
                if(state.terrain[i].type == "terrain"){
                // Here is where I use the midpoint displacement to break down the terrain. 
                    let oldLine = state.terrain[i];
                    let newX = (oldLine.p1.x + oldLine.p2.x) * (1/2);

                    let rg = nextGaussian(-3,3,1);
                    // console.log(rg)
                    let r = s * rg * Math.abs(oldLine.p1.x - oldLine.p2.x)
                    
                    let newY = (1/2) * (oldLine.p1.y + oldLine.p2.y) + r

                    let newLine1 = {
                        p1:{x:oldLine.p1.x,y:oldLine.p1.y},
                        p2:{x:newX,y:newY},
                        type:"terrain"
                    }

                    let newLine2 = {
                        p1:{x:newX,y:newY},
                        p2:{x:oldLine.p2.x,y:oldLine.p2.y},
                        type:"terrain"
                    }

                    // remove old line
                    // state.terrain.splice(i,1);

                    // add new lines
                    // state.terrain.splice(i,0,newLine1,newLine2);

                    tmpTurrain.push(newLine1);
                    tmpTurrain.push(newLine2);
                    
                }
                else{
                    // This is where the line is a pad or edge and needs to be added to the new list to become the new terrain
                    tmpTurrain.push(state.terrain[i])
                }
            }
            s -= .1
            state.terrain = tmpTurrain;

        }

    }

    function processInput(elapsedTime){
        if(state.landerData.theta < 0){
            state.landerData.theta += 2*Math.PI;
        }
        if(state.landerData.theta > 2* Math.PI){
            state.landerData.theta -= 2*Math.PI;
        }
        handleInput(myInput, elapsedTime);
    }

    function generateExplosionParticles(n){
        for(let i = 0; i < n; i++){
            // Math.floor(Math.random() * (max - min) + min);
            let p = {
                type:"fire",
                dimensions:{w:nextGaussian(3,8,1),h:nextGaussian(3,8,1)},
                center:{x:state.landerData.center.x, y:state.landerData.center.y}, // I'll need to move the pixels origin to the exhaust point which is dependent on theta
                theta: Math.random() * (2*Math.PI), // a random direction from 0 to 2 pi
                s_factor:nextGaussian(1.8, 2, 1),
                // rotation:, // I don't know if I'll need this until I see it in action
                lifetime: nextGaussian(200,400,1), // This means the longest living particles life will be x milliseconds
                elapsedLife:0,
            }
            state.particles.push(p);
        }

        for(let i = 0; i < n/4; i++){
            // Math.floor(Math.random() * (max - min) + min);
            let p = {
                type:"smoke",
                dimensions:{w:nextGaussian(3,8,1),h:nextGaussian(3,8,1)},
                center:{x:state.landerData.center.x, y:state.landerData.center.y}, // I'll need to move the pixels origin to the exhaust point which is dependent on theta
                theta: Math.random() * (2*Math.PI), // a random direction from 0 to 2 pi
                s_factor:nextGaussian(1.8, 2, 1),
                // rotation:, // I don't know if I'll need this until I see it in action
                lifetime: nextGaussian(300,500,1), // This means the longest living particles life will be x milliseconds
                elapsedLife:0,
            }
            state.particles.push(p);
        }
    }

    function explode(){
        // This manages how many explosion particles to generate over time 
        if(state.explosionTime + elapsedTime <= 30){
            state.explosionTime += elapsedTime;
        }
        else{
            state.explosionTime += elapsedTime;
            state.explosionTime = state.explosionTime - 30;
            generateExplosionParticles(100);
        }

        if(state.explosionDuration < 300){
            state.explosionDuration += elapsedTime;
        }
        else {
            state.exploding = false;
        }
    }

    function generateThrusterParticles (n){
        // This will generate n particles with some random components to add to the particles list
        let x_offset = -1 * 20 * Math.cos(state.landerData.theta)
        let y_offset =  20 * Math.sin(state.landerData.theta)
        for(let i = 0; i < n; i++){
            // Math.floor(Math.random() * (max - min) + min);
            let rand_neg_pos_factor = Math.random() < 0.5 ? 1: -1;
            let p = {
                type:"thruster",
                dimensions:{w:1,h:1},
                center:{x:state.landerData.center.x + x_offset, y:state.landerData.center.y + y_offset}, // I'll need to move the pixels origin to the exhaust point which is dependent on theta
                theta: state.landerData.theta + Math.PI + Math.random() * rand_neg_pos_factor *(Math.PI/4), // a random direction for the particle to travel on, respective to the landers theta 
                s_factor:nextGaussian(.8, 1.2, 1),
                // rotation:, // I don't know if I'll need this until I see it in action
                lifetime: Math.random() * 400, // This means the longest living particles life will be x milliseconds
                elapsedLife:0,
            }
            state.particles.push(p);
        }

    }



    function processParticles(elapsedTime){
        let unexpiredParticles = []

        // I need to add exploding particles if ship is crashed
        if (state.exploding){
            explode();
        }

        // Iterate over all the particles held in the state and move based on time 
        for(let i = 0; i < state.particles.length; i++){
            // if the lifeElapsed is less than lifetime, then add to next generation of particles
            if(state.particles[i].elapsedLife < state.particles[i].lifetime){
                // update the particles position here
                thrust(state.particles[i].s_factor, state.particles[i].theta, state.particles[i].center); // if this doesn't work I'll make particles there own thrust

                unexpiredParticles.push(state.particles[i])
            }
            state.particles[i].elapsedLife += elapsedTime;
        }
        state.particles = unexpiredParticles;
        // 
    }

    function handleInput(input, elapsedTime){
        // I need to reinforc my control scheme and not just characters
        if (input.keys.hasOwnProperty(state.controls.upMove[0])){
            thrust(1.5, state.landerData.theta, state.landerData.center);
            // I'm debating here adding x number of partices to the particle list for updating and rendering
            // Lets do it and test how well it works.
            generateThrusterParticles(5);
            state.fuel -= 2.5;

            // This is where i'd like to play sound effect for the thruster

            LunarLanding.sounds['thruster'].play()
            state.acceleration = 0;
        }
        if (input.keys.hasOwnProperty(state.controls.leftMove[0])){
            state.landerData.theta += THETA_SLICE * elapsedTime/10;
        }
        if (input.keys.hasOwnProperty(state.controls.rightMove[0])){
            state.landerData.theta -= THETA_SLICE * elapsedTime/10;
        }
    }

    function thrust(v, t, coords,){
        // idea is: newCoord += moveRate * timeIn10thSecs
        // my rate is a small fraction so I don't want it changing by full sec
        coords.x += v * Math.cos(t) * elapsedTime/12
        coords.y += -1 * v * Math.sin(t) * elapsedTime/12

    }


    function initializeGame(elapsedTime){
        // initializeListeners(elapsedTime);
        setLander();
        setPads();
        setTerrain();
        roughTerrain();
        setControls();
        // console.log(state.pads)
    }

    function gravity(elapsedTime){
        // I need to implement some kind of falling acceleration
        // This is bad. This says, if x amount of time has passed move y amount
        // I need instead. Move on a ratio of time.
        if(state.gravityTime + elapsedTime <= 100){
            state.gravityTime += elapsedTime;
        }
        else{
            state.gravityTime += elapsedTime;
            state.gravityTime = state.gravityTime - 100;
            // state.landerData.topLeftPos.y += 1;
            if(state.acceleration < 1){
                state.acceleration += .2;
            }
        }
        state.landerData.center.y += state.acceleration + 1 * elapsedTime/30

        // I wont have acceleration effect the particles because they are "light"
        // This is a lot of computation when there are thousands of particles on screen and not enough effect?
        // if(state.particles.length > 0){
        //     for (let i = 0; i < state.particles; i++){
        //         state.particles[i].center.y + 3 * elapsedTime/30
        //     }
        // }

    }

    // This is the source code provided in the assignment description
    // From observation it takes two points (an object with an x, y coord) and a circle (an object with a center x,y coord and radius)
    function lineCircleIntersection(pt1, pt2, circle) {
        let v1 = { x: pt2.x - pt1.x, y: pt2.y - pt1.y };
        let v2 = { x: pt1.x - circle.center.x, y: pt1.y - circle.center.y };
        let b = -2 * (v1.x * v2.x + v1.y * v2.y);
        let c =  2 * (v1.x * v1.x + v1.y * v1.y);
        let d = Math.sqrt(b * b - 2 * c * (v2.x * v2.x + v2.y * v2.y - circle.radius * circle.radius));
        if (isNaN(d)) { // no intercept
            return false;
        }
        // These represent the unit distance of point one and two on the line
        let u1 = (b - d) / c;  
        let u2 = (b + d) / c;
        if (u1 <= 1 && u1 >= 0) {  // If point on the line segment
            return true;
        }
        if (u2 <= 1 && u2 >= 0) {  // If point on the line segment
            return true;
        }
        return false;
    }

    function setHighScores(){
        let tmpScore1 = 0;
        let permaStore = false;
        //screenHighScore:{state:{highScores
        for(let i = 0; i < LunarLanding.screenHighScore.state.highScores.length; i++){
            if(permaStore == false){
                if (LunarLanding.screenHighScore.state.highScores[i] < LunarLanding.score){
                    permaStore = true;

                    // I found a lower score
                    LunarLanding.screenHighScore.state.highScores.splice(i,0,LunarLanding.score)
                    LunarLanding.screenHighScore.state.highScores.pop();
                }
            }
        }

        if(permaStore == true){
            window.localStorage.setItem('highscores', JSON.stringify(LunarLanding.screenHighScore.state.highScores));
            console.log("From level 2 set new high scores in local storage")
            console.log(JSON.stringify(LunarLanding.screenHighScore.state.highScores));

        }
    }

    function detectCollision(){
        for(let i=0;i<state.terrain.length;i++){
            // This is the condition for the line to be used in the collision detection
            if(state.terrain[i].type == "terrain" || state.terrain[i].type == "pad"){
                // collision with specific line
                // console.log(state.terrain[i])
                if(lineCircleIntersection(state.terrain[i].p1, state.terrain[i].p2, {center:{x:state.landerData.center.x, y:state.landerData.center.y}, radius:state.landerData.radius})){
                    // I need a terrain collision condition
                    if(state.terrain[i].type == "terrain"){
                        state.progress = "game over";
                        LunarLanding.sounds['explosion'].play()
                        state.exploding = true;
                        state.renderShip = false;
                        setTimer();
                    }
                    // I need a pad collision condition
                    else{
                        // I need a safe landing condition
                        // pi/36 rad forgiveness
                        if(state.landerData.theta <= ((19*Math.PI)/36) && state.landerData.theta >= ((17*Math.PI)/36)){
                            // safe angle landing here
                            // console.log("SUCCESSFUL LANDING")
                            // I need a safe speed condition below
                            if(state.speed < .08){
                                // Initiate timer, disable gravity, disable processing Inputs, and navigate at end of timer
                                // The plan is to have a progress property on this screens state and use it to manipulate the update method as described.
                                state.progress = "level complete";
                                LunarLanding.score += Math.floor(state.fuel + (1000 - state.speed * 10000))

                                //Here I will check the high scores for a new insert
                                setHighScores();

                                setTimer();
                            }
                            else{
                                state.progress = "game over";
                                LunarLanding.sounds['explosion'].play()
                                state.exploding = true;
                                state.renderShip = false;
                                setHighScores();
                                setTimer();
                            }
                        }
                        else{
                            state.progress = "game over";
                            LunarLanding.sounds['explosion'].play()
                            state.exploding = true;
                            state.renderShip = false;
                            setHighScores();
                            setTimer();
                        }
                    }
                }
                // no collision with this specific line
                else{

                }
            }
            // condition for not collision
        }
    }

    function setTimer(){
        state.timerInitialized = true;
        state.timer = {count:3, recordedTime:0};
    }

    function deconstructState(){
        state.acceleration = 0;
        state.particles = [];
        state.progress="incomplete";
        state.isInitialized = false;
        state.controls = {};
        state.userInputs = [];
        state.terrain = [];
        state.gravityTime = 0;
        state.explosionTime = 0;
        state.explosionDuration = 0;
        state.landerData = {};
        state.timerInitialized = false;
        state.exploding = false;
        state.renderShip = true;
        state.fuel=1000;
        state.speed;

    }

    function keepTime(elapsedTime){
        if(state.timer.recordedTime + elapsedTime <= 1000){
            state.timer.recordedTime += elapsedTime;
        }
        else{
            state.timer.recordedTime += elapsedTime;
            state.timer.recordedTime = state.timer.recordedTime - 1000;
            if(state.timer.count > 0){
                state.timer.count -= 1;
            }
            else{
                if(state.progress=="level complete"){
                    deconstructState();

                    LunarLanding.sounds['level2Music'].currentTime=2;
                    LunarLanding.sounds['level2Music'].pause()

                    LunarLanding.sounds['homeScreenMusic'].play()

                    navigate("menu");
                }
                else if(state.progress=="game over"){
                    deconstructState();

                    LunarLanding.sounds['level2Music'].currentTime=2;
                    LunarLanding.sounds['level2Music'].pause()

                    LunarLanding.sounds['homeScreenMusic'].play()

                    navigate("menu")
                }
            }
        }
    }

    function navigate(route){
        // event handlers should go away with each input, I think
        // canvas.removeEventListener("click", clickHandler);

        // this way the game will re-initialize when game is restarted. This needs to be addressed. It will break.
        state.isInitialized = false;

        // this is way changes the game screen being displayed.
        LunarLanding.liveScreen = route;
    }

    function update(elapsedTime){
        if(state.progress=="incomplete"){
            // repeating update zone
            if(state.isInitialized==false){
                initializeGame(elapsedTime);
                state.isInitialized = true;
            }

            let tmpPos = {x:state.landerData.center.x,y:state.landerData.center.y};

            if(state.fuel > 0){
                processInput(elapsedTime);
            }
            // After every thing is in place I need to start the physics in motion
            // Here is gravity
            gravity(elapsedTime);
            
            state.speed = Math.sqrt(Math.pow((state.landerData.center.x-tmpPos.x),2)+Math.pow((state.landerData.center.y-tmpPos.y),2))/elapsedTime;

            // This is the method for collision detection
            detectCollision();

            processParticles(elapsedTime);

        }
        else if(state.progress=="game over"){
            processParticles(elapsedTime);
            keepTime(elapsedTime);
        }
        else if(state.progress=="level complete"){
            // game is won, moving to next level
            // needs to update time and on threshold of 3 seconds navigate to next level
            keepTime(elapsedTime);
        }
    }
    // Now all update methods
    return {update}
}(LunarLanding.screenLevel2.state));