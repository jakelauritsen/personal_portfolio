LunarLanding.updateCredits = (function(state){
    canvas = document.getElementById('canvas-main');
    context = canvas.getContext('2d');

    state.isInitialized = false;

    state.userInputs = [];

    COORD_SIZE = 1000;

    let menuButton = {
        "name":"Return to Menu",
        "x":(COORD_SIZE/2)-75,
        "y":(COORD_SIZE/2)-100,
        "width":150,
        "height": 35
    }
    function  getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect(), // abs. size of element
            scaleX = canvas.width / rect.width,    // relationship bitmap vs. element for X
            scaleY = canvas.height / rect.height;  // relationship bitmap vs. element for Y
    
        return {
        x: (evt.clientX - rect.left) * scaleX,   // scale mouse coordinates after they have
        y: (evt.clientY - rect.top) * scaleY     // been adjusted to be relative to element
        }
    }

    //Function to check whether a point is inside a rectangle
    function isInside(pos, rect){
        // console.log(pos)
        // console.log(rect)
        return (pos.x > rect.x && pos.x < rect.x+rect.width && pos.y < rect.y+rect.height && pos.y > rect.y)
    }

    let clickHandler = function(evt) {
        // canvas = document.getElementById('canvas-main');
        // context = canvas.getContext('2d');

        var mousePos = getMousePos(canvas, evt);
    
        if (isInside(mousePos, menuButton)) {
            state.userInputs.push('menu')
            LunarLanding.sounds['button'].play();
        }
        else{
            // console.log("Miss")
        }
    }

    function initializeListeners(){
        canvas.addEventListener('click', clickHandler, false);
    }

    function navMenu(){
        canvas.removeEventListener("click", clickHandler);
        state.isInitialized = false;
        LunarLanding.liveScreen = "menu";
        // console.log(LunarLanding)
    }

    function handleUserInputs(){
        if(state.userInputs.length > 0){
            // list has inputs to handle
            for(let i = 0; i < state.userInputs.length; i++){
                if(state.userInputs == 'menu'){
                    state.userInputs.splice(i,1);
                    // console.log("navigation function called")
                    navMenu();
                }
                
                else{
                    console.log('unrecognized user input')
                }
            }
        }
    }



    function update(elapsedTime){
        // repeating update zone
        if(state.isInitialized==false){
            initializeListeners();
            state.isInitialized = true;
        }

        // Now I need to handle user inputs, in this case is just the return to menu button 
        handleUserInputs();

    }
    // Now all update methods
    return {update}
}(LunarLanding.screenCredits.state));