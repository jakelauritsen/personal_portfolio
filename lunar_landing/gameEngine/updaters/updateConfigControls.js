LunarLanding.updateConfigControls = (function(state){
    canvas = document.getElementById('canvas-main');
    context = canvas.getContext('2d');

    state.isInitialized = false;

    state.userInputs = [];

    COORD_SIZE = 1000;

    function keyHandler(e) {
        // console.log(`${e.key} : ${e.code}`);
        state.userInputs.push(e.key)
        // console.log(state.userInputs)
    }

    function initializeListeners(){
        window.addEventListener('keyup', keyHandler, false);
        // console.log("Init config control listener")
    }

    function navControls(){
        window.removeEventListener("keyup", keyHandler);
        state.isInitialized = false;
        LunarLanding.liveScreen = "Controls";
        state.userInputs = [];
        // console.log("Deconstructed config control screen, navigate to controls menu")
    }

    function handleUserInputs(){
        if(state.userInputs.length >= 3){
            // list has inputs to handle
            // Here I need to change the state of the controls for the screenControls
            // order NewUpKey -> NewLeftKey -> NewRightKey
            LunarLanding.screenControls.state.controls.upMove = state.userInputs[0];
            LunarLanding.screenControls.state.controls.leftMove = state.userInputs[1];
            LunarLanding.screenControls.state.controls.rightMove = state.userInputs[2];
            navControls();
        }
    }



    function update(elapsedTime){
        // repeating update zone
        if(state.isInitialized==false){
            initializeListeners();
            state.isInitialized = true;
        }

        // Now I need to handle user inputs, in this case is just the return to menu button 
        handleUserInputs();

    }
    // Now all update methods
    return {update}
}(LunarLanding.screenConfigControls.state));