LunarLanding.updateMenu = (function(state){
    canvas = document.getElementById('canvas-main');
    context = canvas.getContext('2d');

    state.isInitialized = false;
    state.userInputs = [];
    state.soundInitilized = false;

    COORD_SIZE = 1000;

    state.buttons = [{
        "name":"New Game",
        "x":(COORD_SIZE/2)-75,
        "y":(COORD_SIZE/2)-100,
        "width":150,
        "height": 35,
    },
    {
        "name":"High Scores",
        "x":(COORD_SIZE/2)-75,
        "y":(COORD_SIZE/2)-55,
        "width":150,
        "height": 35,
    },
    {
        "name":"Controls",
        "x":(COORD_SIZE/2)-75,
        "y":(COORD_SIZE/2)-10,
        "width":150,
        "height": 35,
    },
    {
        "name":"Credits",
        "x":(COORD_SIZE/2)-75,
        "y":(COORD_SIZE/2)+35,
        "width":150,
        "height": 35,
    }]



    // The clicking events for the buttons need to push the different sized mazes into the event list. After a click the event listenter needs to be removed.


    function  getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect(), // abs. size of element
            scaleX = canvas.width / rect.width,    // relationship bitmap vs. element for X
            scaleY = canvas.height / rect.height;  // relationship bitmap vs. element for Y
    
        return {
        x: (evt.clientX - rect.left) * scaleX,   // scale mouse coordinates after they have
        y: (evt.clientY - rect.top) * scaleY     // been adjusted to be relative to element
        }
    }

    //Function to check whether a point is inside a rectangle
    function isInside(pos, rect){
        // console.log(pos)
        // console.log(rect)
        return (pos.x > rect.x && pos.x < rect.x+rect.width && pos.y < rect.y+rect.height && pos.y > rect.y)
    }

    function navNewGame(){
        // This function will be triggered when the button is clicked and change the state of the program so that the credits screen is rendered.
        // I'm leaving the menu so I need to remove the click listeneres
        // console.log("Killed the listener")
        canvas.removeEventListener("click", clickHandler);
        state.isInitialized = false;
        LunarLanding.liveScreen = "Level1";
        LunarLanding.sounds['homeScreenMusic'].currentTime=0;
        LunarLanding.sounds['homeScreenMusic'].pause()

        LunarLanding.sounds['level1Music'].currentTime=2;
        LunarLanding.sounds['level1Music'].play();

    }

    function navHighScores(){
        canvas.removeEventListener("click", clickHandler);
        state.isInitialized = false;
        LunarLanding.liveScreen = "High Scores";
    }

    function navControls(){
        canvas.removeEventListener("click", clickHandler);
        state.isInitialized = false;
        LunarLanding.liveScreen = "Controls";
    }

    function navCredits(){
        canvas.removeEventListener("click", clickHandler);
        state.isInitialized = false;
        LunarLanding.liveScreen = "credits";
        // console.log("Killed the listener")

    }


    let soundHandler = function(){
        state.soundInitilized == true;
        LunarLanding.sounds['homeScreenMusic'].play()
        window.removeEventListener('click', soundHandler)
    }

    let clickHandler = function(evt) {
        // canvas = document.getElementById('canvas-main');
        // context = canvas.getContext('2d');

        var mousePos = getMousePos(canvas, evt);
    
        if (isInside(mousePos, state.buttons[0])) {
            state.userInputs.push('New Game')
            LunarLanding.sounds['button'].play()

        }
        else if (isInside(mousePos, state.buttons[1])){
            //console.log(eventList[0].background.buttons[0])
            state.userInputs.push("High Scores")
            LunarLanding.sounds['button'].play()

        }   
        else if (isInside(mousePos, state.buttons[2])){
            state.userInputs.push("Controls")
            LunarLanding.sounds['button'].play()

        }
        else if (isInside(mousePos, state.buttons[3])){
            state.userInputs.push("Credits")
            LunarLanding.sounds['button'].play()

        }
    }

    
    // I need to track if the menu's listeners have been initialized in the game state
    function initializeListeners(){
        LunarLanding.sounds['homeScreenMusic'].play();
        canvas.addEventListener('click', clickHandler, false);
        window.addEventListener('click', soundHandler)
    }
    
    function handleUserInputs(){
        if(state.userInputs.length > 0){
            // list has inputs to handle
            for(let i = 0; i < state.userInputs.length; i++){
                if(state.userInputs == 'New Game'){
                    state.userInputs.splice(i,1);
                    navNewGame();
                }
                else if(state.userInputs == 'High Scores'){
                    state.userInputs.splice(i,1);
                    navHighScores();
                }
                else if(state.userInputs == 'Controls'){
                    state.userInputs.splice(i,1);
                    navControls();
                }
                else if(state.userInputs == 'Credits'){
                    state.userInputs.splice(i,1);
                    navCredits();
                }
                else{
                    console.log('unrecognized user input')
                }
            }
        }
    }

    // function playSound(whichSound) {

    //     // LunarLanding.sounds[whichSound].addEventListener('ended', function() {
    //     //     elementButton.onclick = function() { playSound(whichSound, label, idButton, idStatus); };
    //     // });
    
    //     LunarLanding.sounds[whichSound].play();
    // }
    


    function update(elapsedTime){
        // First check for initialization
        if(state.isInitialized==false){
            // console.log("Ran listener init")
            initializeListeners();
            state.isInitialized = true;
        }

        // Second check for user inputs 
        handleUserInputs();
        
        // This menuUpdate is for tracking the state of the menu. Does the menu have anything dynamic? What data will the renderMenu need? 
        // It will include 4 navigation buttons to 'New Game', 'High Scores', 'Controls', 'Credits'
        
        // I need to add some event listeners for handling clicking the boxes.
        // I have kind of been ignoring using a seperate user input handler. User inputs should be the first thing in update functions
            //Function to get the mouse position


    }
    // Now all update methods
    return {update}
}(LunarLanding.screenMenu.state));