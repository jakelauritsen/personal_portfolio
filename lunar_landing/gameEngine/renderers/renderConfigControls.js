LunarLanding.renderConfigControls = (function(state){
    // This must be the first thing in hte update method to preserve the processInput -> update -> render, model
    // processInput();

    // let state = LunarLanding.state;
    let COORD_SIZE = 1000

    let menuButton = {
        "name":"Return to Menu",
        "x":(COORD_SIZE/2)-75,
        "y":(COORD_SIZE/2)-100,
        "width":150,
        "height": 35
    }

    // how to load background
    let background = new Image();
    background.isReady = false;

    background.onload = function() {
        console.log("Image was loaded")
        this.isReady = true;
    };
    
    background.src = "gameEngine/images/controls_background.png";

    function render(context, canvas){
        // Render the background
        if (background.isReady) {
            context.drawImage(background, 0, 0, COORD_SIZE , COORD_SIZE);
            console.log("Drew background")
        }

        // Here tells the user to perform configuring keystrokes
        context.save();
    
        context.strokeStyle = 'rgb(255, 255, 255)';
        context.lineWidth = 6;
    
        context.font = "30px Sans-Serif";
        context.fillStyle = 'rgb(144, 255, 255)';
        context.textAlign = "center";
    
        context.fillText("Type your new control layout in order as follows:", COORD_SIZE/2, COORD_SIZE/4+80); 
        context.fillText("NewUpKey -> NewLeftKey -> NewRightKey", COORD_SIZE/2, COORD_SIZE/4+110); 
        context.fillText("You'll return to the control menu when complete", COORD_SIZE/2, COORD_SIZE/4+140); 
        
        context.restore();
    }
    // Now all update methods
    return {render}
}(LunarLanding.screenConfigControls.state));