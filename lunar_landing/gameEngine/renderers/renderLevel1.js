LunarLanding.renderLevel1 = (function(state){
    // This must be the first thing in hte update method to preserve the processInput -> update -> render, model
    // processInput();

    // let state = LunarLanding.state;
    let COORD_SIZE = 1000


    // how to load background
    let background = new Image();
    background.isReady = false;

    background.onload = function() {
        console.log("Image was loaded")
        this.isReady = true;
    };

    let lander = new Image();
    background.isReady = false;

    lander.onload = function() {
        console.log("Image was loaded")
        this.isReady = true;
    };

    let fire = new Image();
    fire.isReady = false;

    fire.onload = function() {
        console.log("Image was loaded")
        this.isReady = true;
    };

    let smoke = new Image();
    smoke.isReady = false;
    smoke.onload = function() {
        console.log("Image was loaded")
        this.isReady = true;
    };

    lander.src = "gameEngine/images/transparent_lander.png";
    background.src = "gameEngine/images/level1_background.jpg";
    fire.src = "gameEngine/images/fire.png";
    smoke.src = "gameEngine/images/smoke-2.png";

    function render(context, canvas){
        if(state.isInitialized){
            // Render the background
            if (background.isReady) {
                context.drawImage(background, 0, 0, COORD_SIZE , COORD_SIZE);
                // console.log("Drew background")
            }

            // I need to rotate the image according to the landers theta, confirmed rotate uses theta
            // I need to translate the canvas to the center of image
            // I then need to rotate
            // render image *****ABOUT THE CANVAS -width*1/2, -height*1/2 POSITION***********
            // This nearly murdered me ^^^^^^^^^^^^^^^^^^^^^^^^
            // restore
            // I tried to rotate based on a calculated center and it kind of crashed and burned so I'll just calculate here
            
            if (state.renderShip){
                if (lander.isReady) {
                    context.save();
                    context.translate(state.landerData.center.x, state.landerData.center.y);
                    context.rotate(-1*(state.landerData.theta-(Math.PI/2)));
                    context.drawImage(lander, -1*state.landerData.dimensions.w/2, -1*state.landerData.dimensions.h/2, state.landerData.dimensions.w, state.landerData.dimensions.h);
                    context.restore();
                }
            }
                // This commented out code renders the calculated circle around the ship used for collision detection debugging
                // context.arc(x,y,r,sAngle,eAngle,counterclockwise);

                // context.save();
                // context.beginPath();
                // context.arc(state.landerData.center.x, state.landerData.center.y, state.landerData.radius, 0, 2 * Math.PI);
    
                // context.lineWidth = 2;
                // context.strokeStyle = 'rgb(0, 0, 255)';
    
                // context.stroke(); 
                // context.closePath();
                // context.restore();

                // console.log("Drew background")

            context.save();
            context.beginPath();
        

            context.moveTo(state.terrain[0].p1.x, state.terrain[0].p1.y);

            for(let i = 0; i < state.terrain.length; i++){
                context.lineTo(state.terrain[i].p2.x, state.terrain[i].p2.y);
            }

            context.strokeStyle = 'rgb(0, 0, 0)';
            context.lineWidth = 4;
            context.fillStyle = 'rgb(80,100,140)';
            context.fill();
            // context.closePath();
            context.stroke();
            context.restore();

            // This will render the particles if the particles list in the state is not empty
            if(state.particles.length > 0){
                // here render a square for each particle object
                for(let i = 0; i < state.particles.length; i++){
                    if (state.particles[i].type=="thruster"){
                        context.save();
                        context.strokeStyle = "rgb(255,255,255)";
                        context.strokeRect(state.particles[i].center.x, state.particles[i].center.y, state.particles[i].dimensions.w, state.particles[i].dimensions.h);
                        context.restore();
                    }
                    else if (state.particles[i].type=="fire"){
                        if(fire.isReady){
                            context.drawImage(fire, state.particles[i].center.x, state.particles[i].center.y, state.particles[i].dimensions.w , state.particles[i].dimensions.h);
                        }
                    }
                    else if (state.particles[i].type=="smoke"){
                        if(smoke.isReady){
                            context.drawImage(smoke, state.particles[i].center.x, state.particles[i].center.y, state.particles[i].dimensions.w , state.particles[i].dimensions.h);
                        }
                    }
                }
            }

            // This renders the timer
            if (state.timerInitialized){
                if(state.progress=="level complete"){
                context.save();
                context.font = "40px Sans-Serif";
                context.fillStyle = 'rgb(255, 255, 255)';
                context.textAlign = "center";
                
                let successStatement = "...The Eagle has Landed...Redirecting in : " + state.timer.count;
                context.fillText(successStatement, canvas.width/2, canvas.height*.25);
                context.restore();
                }
                else if(state.progress=="game over"){
                    context.save();
                    context.font = "40px Sans-Serif";
                    context.fillStyle = 'rgb(255, 255, 255)';
                    context.textAlign = "center";
                    
                    let successStatement = "...Luckily, it's just a simulation...Redirecting in : " + state.timer.count;
                    context.fillText(successStatement, canvas.width/2, canvas.height*.25);
                    context.restore();
                }
            }

            // This will render the hud
            context.save();
            context.strokeStyle = "rgb(255,255,255)";
            context.strokeRect(COORD_SIZE*.75, COORD_SIZE*.05, 200, 115);
            context.restore();

            context.save();
            let fuelWarning = Math.floor(state.fuel/1000*255).toString();
            context.font = "20px Sans-Serif";
            context.fillStyle = "rgb(255,"+fuelWarning+","+fuelWarning+")";
            context.fillText("Fuel: "+ Math.floor(state.fuel/10) + "%", canvas.width*.75+20, canvas.height*.05+25);
            context.restore();

            context.save();
            context.fillStyle = "rgb(255,255,255)";
            context.font = "20px Sans-Serif";
            context.fillText("Orientation: "+ Math.floor(state.landerData.theta*180*1/Math.PI) + " deg", canvas.width*.75+20, canvas.height*.05+50);
            context.fillText("Speed: "+ (state.speed*100).toPrecision(2) + " m/s", canvas.width*.75+20, canvas.height*.05+75);
            context.fillText("Score: "+ LunarLanding.score, canvas.width*.75+20, canvas.height*.05+100);
            context.restore();

        }
        else{
            if (background.isReady) {
                context.drawImage(background, 0, 0, COORD_SIZE , COORD_SIZE);
                // console.log("Drew background")
            }
        }


    }
    // Now all update methods
    return {render}
}(LunarLanding.screenLevel1.state));