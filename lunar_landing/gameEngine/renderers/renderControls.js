LunarLanding.renderControls = (function(state){
    // This must be the first thing in hte update method to preserve the processInput -> update -> render, model
    // processInput();

    // let state = LunarLanding.state;
    let COORD_SIZE = 1000

    let buttons = [{
        "name":"Return to Menu",
        "x":(COORD_SIZE/2)-75,
        "y":(COORD_SIZE/2),
        "width":150,
        "height": 35
    },
    {
        "name":"Configure Controls",
        "x":(COORD_SIZE/2)-75,
        "y":(COORD_SIZE/2) + 45,
        "width":150,
        "height": 35
    }]

    // how to load background
    let background = new Image();
    background.isReady = false;

    background.onload = function() {
        console.log("Image was loaded")
        this.isReady = true;
    };
    
    background.src = "gameEngine/images/controls_background.png";

    function render(context, canvas){
        // if(state.controlConfigLive){
        //     context.save();
        
        //     context.strokeStyle = 'rgb(255, 255, 255)';
        //     context.lineWidth = 6;
        
        //     context.font = "30px Sans-Serif";
        //     context.fillStyle = 'rgb(144, 255, 255)';
        //     context.textAlign = "center";
        
        //     context.fillText("Type your new control layout in order as follows:\n NewUpKey -> NewDownKey -> NewLeftKey -> NewRightKey", COORD_SIZE/2, COORD_SIZE/4+80); 
        
        //     context.restore();
        // }
        // else {

        
        // Render the background
        if (background.isReady) {
            context.drawImage(background, 0, 0, COORD_SIZE , COORD_SIZE);
            console.log("Drew background")
        }

        context.save();
    
        context.strokeStyle = 'rgb(255, 255, 255)';
        context.lineWidth = 6;
    
        context.font = "50px Sans-Serif";
        context.fillStyle = 'rgb(144, 255, 255)';
        context.textAlign = "center";
    
        context.fillText("Current Control Scheme", COORD_SIZE/2, COORD_SIZE/4+80); 
    
        context.restore();

        let offset = 30;

        context.save();
    
        context.strokeStyle = 'rgb(255, 255, 255)';
        context.lineWidth = 6;
        
        context.font = "30px Sans-Serif";
        context.fillStyle = 'rgb(144, 255, 255)';
        context.textAlign = "center";
        
        tmpStr = 'Thruster' + ': ' + state.controls.upMove[0];
        context.fillText(tmpStr, COORD_SIZE/2, COORD_SIZE/4+120+(1*offset)); 
    
        context.restore();

        // context.save();
    
        // context.strokeStyle = 'rgb(255, 255, 255)';
        // context.lineWidth = 6;
        
        // context.font = "30px Sans-Serif";
        // context.fillStyle = 'rgb(144, 255, 255)';
        // context.textAlign = "center";
        
        // tmpStr = 'Downward Movement key' + ': ' + state.controls.downMove[0];
        // context.fillText(tmpStr, COORD_SIZE/2, COORD_SIZE/4+120+(1*offset)); 
    
        // context.restore();

        context.save();
    
        context.strokeStyle = 'rgb(255, 255, 255)';
        context.lineWidth = 6;
        
        context.font = "30px Sans-Serif";
        context.fillStyle = 'rgb(144, 255, 255)';
        context.textAlign = "center";
        
        tmpStr = 'Rotate Counter Clockwise' + ': ' + state.controls.leftMove[0];
        context.fillText(tmpStr, COORD_SIZE/2, COORD_SIZE/4+120+(2*offset)); 
    
        context.restore();

        context.save();
    
        context.strokeStyle = 'rgb(255, 255, 255)';
        context.lineWidth = 6;
        
        context.font = "30px Sans-Serif";
        context.fillStyle = 'rgb(144, 255, 255)';
        context.textAlign = "center";
        
        tmpStr = 'Rotate Clockwise' + ': ' + state.controls.rightMove[0];
        context.fillText(tmpStr, COORD_SIZE/2, COORD_SIZE/4+120+(3*offset)); 
    
        context.restore();

        // Render the return to menu button, then handle the navigation

        for(let i = 0; i < buttons.length; i++){
            context.save();
            context.fillStyle = "rgb(255,0,255)";
            context.fillRect(buttons[i].x, buttons[i].y, buttons[i].width, buttons[i].height);
    
            context.strokeStyle = "rgb(144,255,255)";
            context.strokeRect(buttons[i].x, buttons[i].y, buttons[i].width, buttons[i].height);
            context.restore();
    
            context.save();
            context.strokeStyle = 'rgb(255, 255, 255)';
            context.lineWidth = 6;
        
            context.font = "16px Sans-Serif";
            context.fillStyle = 'rgb(144, 255, 255)';
            context.textAlign = "center";
        
            context.fillText(buttons[i].name, buttons[i].x+75, buttons[i].y+25);
            context.restore(); 
        }
        // }
    }
    // Now all update methods
    return {render}
}(LunarLanding.screenControls.state));