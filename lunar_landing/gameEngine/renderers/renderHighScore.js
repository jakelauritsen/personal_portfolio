LunarLanding.renderHighScore = (function(state){
    // This must be the first thing in hte update method to preserve the processInput -> update -> render, model
    // processInput();

    // let state = LunarLanding.state;
    let COORD_SIZE = 1000

    let menuButton = {
        "name":"Return to Menu",
        "x":(COORD_SIZE/2)-75,
        "y":(COORD_SIZE/2),
        "width":150,
        "height": 35
    }

    // how to load background
    let background = new Image();
    background.isReady = false;

    background.onload = function() {
        console.log("Image was loaded")
        this.isReady = true;
    };
    
    background.src = "gameEngine/images/high_scores_background.jpg";

    function render(context, canvas){
        // Render the background
        if (background.isReady) {
            context.drawImage(background, 0, 0, COORD_SIZE , COORD_SIZE);
            console.log("Drew background")
        }

        context.save();
    
        context.strokeStyle = 'rgb(255, 255, 255)';
        context.lineWidth = 6;
    
        context.font = "50px Sans-Serif";
        context.fillStyle = 'rgb(144, 255, 255)';
        context.textAlign = "center";
    
        context.fillText("Top 5 High Scores", COORD_SIZE/2, COORD_SIZE/4+80); 
    
        context.restore();

        let offset = 30;
        for(let i = 0; i < state.highScores.length; i++){
            context.save();
        
            context.strokeStyle = 'rgb(255, 255, 255)';
            context.lineWidth = 6;
            
            context.font = "30px Sans-Serif";
            context.fillStyle = 'rgb(144, 255, 255)';
            context.textAlign = "center";
            
            tmpStr = (i+1).toString() + ': ' + state.highScores[i].toString();
            context.fillText(tmpStr, COORD_SIZE/2, COORD_SIZE/4+120+(i*offset)); 
        
            context.restore();
        }
        // Render the return to menu button, then handle the navigation
        context.save();
        context.fillStyle = "rgb(255,0,255)";
        context.fillRect(menuButton.x, menuButton.y, menuButton.width, menuButton.height);

        context.strokeStyle = "rgb(144,255,255)";
        context.strokeRect(menuButton.x, menuButton.y, menuButton.width, menuButton.height);
        context.restore();

        context.save();
        context.strokeStyle = 'rgb(255, 255, 255)';
        context.lineWidth = 6;
    
        context.font = "16px Sans-Serif";
        context.fillStyle = 'rgb(144, 255, 255)';
        context.textAlign = "center";
    
        context.fillText(menuButton.name, menuButton.x+75, menuButton.y+25);
        context.restore(); 

    }
    // Now all update methods
    return {render}
}(LunarLanding.screenHighScore.state));