LunarLanding.renderMenu = (function(state){
    // This must be the first thing in hte update method to preserve the processInput -> update -> render, model
    // processInput();

    // let state = LunarLanding.state;
    let COORD_SIZE = 1000

    let buttons = [{
        "name":"New Game",
        "x":(COORD_SIZE/2)-75,
        "y":(COORD_SIZE/2)-100,
        "width":150,
        "height": 35,
    },
    {
        "name":"High Scores",
        "x":(COORD_SIZE/2)-75,
        "y":(COORD_SIZE/2)-55,
        "width":150,
        "height": 35,
    },
    {
        "name":"Controls",
        "x":(COORD_SIZE/2)-75,
        "y":(COORD_SIZE/2)-10,
        "width":150,
        "height": 35,
    },
    {
        "name":"Credits",
        "x":(COORD_SIZE/2)-75,
        "y":(COORD_SIZE/2)+35,
        "width":150,
        "height": 35,
    }]


    let background = new Image();
    
    background.isReady = false;

    background.onload = function() {
        // console.log("Image was loaded")
        this.isReady = true;
    };
    
    background.src = "gameEngine/images/menu_background.png";

    function render(context, canvas){
        // The masterUpdate could act as the navigator checking the state for a current screen to update
        // updating events that navigate between screens need to appropriately remove current event listeners

        // I need to apply a background.
        context.save();

        if (background.isReady) {
            context.drawImage(background, 0, 0, COORD_SIZE , COORD_SIZE);
            // console.log("Drew background")
        }

        context.restore();

        // I need to re-implement everything so it scales with the window, before moving further.

        // I need to add event listeners.

        context.save();
    
        context.strokeStyle = 'rgb(255, 255, 255)';
        context.lineWidth = 6;
    
        context.font = "50px Sans-Serif";
        context.fillStyle = 'rgb(144, 255, 255)';
        context.textAlign = "center";
    
        context.fillText("Lunar Landing", COORD_SIZE/2, COORD_SIZE/4+80); 
    
        context.restore();
    
        // Create all the buttons using the menu object in the eventList
        for(let i = 0; i < 4; i++){
            context.save();
            context.fillStyle = "rgb(255,0,255)";
            context.fillRect(buttons[i].x, buttons[i].y, buttons[i].width, buttons[i].height);
    
            context.strokeStyle = "rgb(144,255,255)";
            context.strokeRect(buttons[i].x, buttons[i].y, buttons[i].width, buttons[i].height);
            context.restore();
    
            context.save();
            context.strokeStyle = 'rgb(255, 255, 255)';
            context.lineWidth = 6;
        
            context.font = "16px Sans-Serif";
            context.fillStyle = 'rgb(144, 255, 255)';
            context.textAlign = "center";
        
            context.fillText(buttons[i].name, buttons[i].x+75, buttons[i].y+25);
            context.restore(); 
        }
        
    }
    // Now all update methods
    return {render}
}(LunarLanding.screenMenu.state));