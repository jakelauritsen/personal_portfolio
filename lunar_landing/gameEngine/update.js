// This is an IIFE function. This is similar to an anonomous function that calls its with the paramaters in the last '()'
LunarLanding.update = (function(){
    // This must be the first thing in hte update method to preserve the processInput -> update -> render, model
    // processInput();

    // let state = LunarLanding.state;
    LunarLanding.score = 0;
    console.log(-5%2);

    // window.localStorage.clear();

    // The idea of adding the loadSound function here is through javascripts weird scoping which I forgot the term for, the other updates will be able to load sound
    function loadSound(source) {
        let sound = new Audio();
        sound.src = source;
        return sound;
    }

    function loadAudio() {
        LunarLanding.sounds = {}
        // Reference: https://freesound.org/data/previews/156/156031_2703579-lq.mp3

        // the loadSound function simply adds the eventListeners to the audio object, which just print statuses, I think.
        
        LunarLanding.sounds['homeScreenMusic'] = loadSound('gameEngine/audio/home_soundtrack.mp3');
        LunarLanding.sounds['homeScreenMusic'].loop=true;

        LunarLanding.sounds['level1Music'] = loadSound('gameEngine/audio/level1_soundtrack.mp3');
        LunarLanding.sounds['level1Music'].loop=true;

        LunarLanding.sounds['level2Music'] = loadSound('gameEngine/audio/level2_soundtrack.mp3');
        LunarLanding.sounds['level2Music'].loop=true;

        LunarLanding.sounds['explosion'] = loadSound('gameEngine/audio/exploding_sfx.mp3');

        LunarLanding.sounds['button'] = loadSound('gameEngine/audio/button_sfx.mp3');

        LunarLanding.sounds['thruster'] = loadSound('gameEngine/audio/shorter_thruster_sfx0001.mp3');
        LunarLanding.sounds['thruster'].volume=.1;
        // // Reference: https://freesound.org//data/previews/109/109662_945474-lq.mp3
        // LunarLanding.sounds['audio/sound-2'] = loadSound('audio/sound-2.mp3', 'Sound 2', 'id-play2');
        // // Reference: https://www.bensound.com/royalty-free-music/track/extreme-action
        // LunarLanding.sounds['audio/bensound-extremeaction'] = loadSound('audio/bensound-extremeaction.mp3', 'Music', 'id-play3');
    }

    console.log('initializing...');

    loadAudio();


    function masterUpdate(elapsedTime){
        // The masterUpdate could act as the navigator checking the state for a current screen to update
        // updating events that navigate between screens need to appropriately remove current event listeners
        if(LunarLanding.liveScreen == 'menu'){
            LunarLanding.updateMenu.update(elapsedTime);
        }
        else if(LunarLanding.liveScreen == 'credits'){
            LunarLanding.updateCredits.update(elapsedTime);
        }
        else if(LunarLanding.liveScreen == 'High Scores'){
            LunarLanding.updateHighScore.update(elapsedTime);
        }
        else if(LunarLanding.liveScreen == 'Controls'){
            LunarLanding.updateControls.update(elapsedTime);
        }
        else if(LunarLanding.liveScreen == 'configControls'){
            LunarLanding.updateConfigControls.update(elapsedTime);
        }
        else if(LunarLanding.liveScreen == 'Level1'){
            LunarLanding.updateLevel1.update(elapsedTime);
        }
        else if(LunarLanding.liveScreen == 'Level2'){
            LunarLanding.updateLevel2.update(elapsedTime);
        }
    }
    // Now all update methods
    return {masterUpdate}
}());
