LunarLanding.core = (function(update, render){
    // Anything here I think will work similarly to an initialize
    console.log(LunarLanding)
    lastStamp = 0;

    function gameLoop(){
        // In order to preserve the idea that update should know how to update itself and be completely capsulated I will calculate a time elapsed within.
        //      I might have to append a last time property to the game model for that.

        presentTime = performance.now();
        elapsedTime = presentTime - lastStamp;
        lastStamp = presentTime;

        update.masterUpdate(elapsedTime);
        
        // In class Dr. Mathias stated that a render can be given a "game model" and then should be designed to render that models state
        render.masterRender();

        requestAnimationFrame(gameLoop);
    }
    requestAnimationFrame(gameLoop);

}(LunarLanding.update, LunarLanding.render));


